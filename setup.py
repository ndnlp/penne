#!/usr/bin/env python

from setuptools import setup

setup(name='penne',
      version='0.2',
      description='Python easy neural network extruder',
      author='David Chiang',
      author_email='dchiang@nd.edu',
      url='https://bitbucket.org/ndnlp/penne',
      license='MIT',
      packages=['penne'],
      install_requires=['numpy', 'scipy', 'six'],
      )
