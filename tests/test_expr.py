import unittest
import numpy
import penne

import argparse
argparser = argparse.ArgumentParser()
argparser.add_argument('--gpu', dest='gpu', action='store_true')
args = argparser.parse_args()

if args.gpu:
    import pygpu
    penne.use_gpu()
    penne.backend.set_max_bits(None)

def generate(shape, dtype=numpy.float64):
    na = numpy.random.uniform(0.9, 1.1, shape).astype(dtype)
    pa = penne.parameter(na)
    return na, pa

def check(na, pa):
    pr = penne.sum(pa * penne.constant(numpy.random.uniform(0.9, 1.1, na.shape).astype(na.dtype)))
    if not numpy.allclose(na, pa.value, rtol=0.01):
        print('(forward) values differ:')
        print(na)
        print(pa.value)
        print((na-pa.value)/na)
        return False
    penne.check_gradients(pr, delta=1e-4, threshold=0.01)
    return True

class IndexingTestCase(unittest.TestCase):
    def test_take(self):
        dtype = numpy.float64
        for d in [1,2,3]:
            na, pa = generate((5,)*d, dtype)
            for idx in [1, [1,3,1], [[1,3],[3,1]]]:
                for axis in [None] + list(range(d)):
                    self.assertTrue(check(numpy.take(na, idx, axis), penne.take(pa, idx, axis)))

class ComponentwiseTestCase(unittest.TestCase):
    def test_arithmetic(self):
        dtype = numpy.float64
        na, pa = generate((2,3,4), dtype)
        nb, pb = generate((2,3,4), dtype)
        self.assertTrue(check(+na, +pa))
        self.assertTrue(check(-na, -pa))
        self.assertTrue(check(na+nb, pa+pb))
        self.assertTrue(check(na-nb+1, pa-pb+penne.constant(1)))
        self.assertTrue(check(na*nb, pa*pb))
        self.assertTrue(check(na/(nb+1), pa/(pb+penne.constant(1))))
        self.assertTrue(check(na**nb, pa**pb))

    def test_transcendental(self):
        dtype = numpy.float64
        na, pa = generate((2,3,4), dtype)
        self.assertTrue(check(numpy.exp(na), penne.exp(pa)))
        self.assertTrue(check(numpy.log(na+1), penne.log(pa+penne.constant(1))))
        self.assertTrue(check(numpy.tanh(na), penne.tanh(pa)))

    def test_comparison(self):
        dtype = numpy.float64
        na, pa = generate((2,3,4), dtype)
        nb, pb = generate((2,3,4), dtype)
        self.assertTrue(check(numpy.maximum(na, nb), penne.maximum(pa, pb)))
        self.assertTrue(check(numpy.minimum(na, nb), penne.minimum(pa, pb)))

class ReductionTestCase(unittest.TestCase):
    def test_sum(self):
        dtype = numpy.float64
        na, pa = generate((2,3,4), dtype)

        self.assertTrue(check(numpy.sum(na), penne.sum(pa)))
        self.assertTrue(check(numpy.sum(na, axis=0), penne.sum(pa, axis=0)))
        self.assertTrue(check(numpy.sum(na, axis=1), penne.sum(pa, axis=1)))
        self.assertTrue(check(numpy.sum(na, axis=2), penne.sum(pa, axis=2)))
        self.assertTrue(check(numpy.sum(na, axis=(0,2)), penne.sum(pa, axis=(0,2))))
        self.assertTrue(check(numpy.sum(na, axis=0, keepdims=True), penne.sum(pa, axis=0, keepdims=True)))
        self.assertTrue(check(numpy.sum(na, axis=(0,2), keepdims=True), penne.sum(pa, axis=(0,2), keepdims=True)))

    def test_vecdot(self):
        dtype = numpy.float64
        na, pa = generate((2,3), dtype)
        nb, pb = generate((2,3), dtype)

        self.assertTrue(check(numpy.sum(na*nb, keepdims=True), penne.vecdot(pa, pb, keepdims=True)))

class ManipulationTestCase(unittest.TestCase):
    def test_concatenate(self):
        na, pa = generate((3, 4))
        nb, pb = generate((3, 5))
        nc = numpy.concatenate([na, nb], axis=1)
        pc = penne.concatenate([pa, pb], axis=1)
        self.assertTrue(check(nc, pc))

    def test_transpose(self):
        na, pa = generate((3,4,5,6))
        nb = numpy.transpose(na, [1,3,0,2])
        pb = penne.transpose(pa, [1,3,0,2])
        self.assertTrue(check(nb, pb))

class DotTestCase(unittest.TestCase):
    def test_dot(self):
        for nd1 in [1,2,3,4]:
            for nd2 in [1,2,3,4]:
                shp1 = range(2, 2+nd1)
                if nd2 == 1:
                    shp2 = (1+nd1,)
                else:
                    shp2 = list(range(2, 2+nd2))
                    shp2[-2] = shp1[-1]
                    shp2 = tuple(shp2)
                na, pa = generate(shp1)
                nb, pb = generate(shp2)
                nc = numpy.dot(na, nb)
                pc = penne.dot(pa, pb)
                self.assertTrue(check(nc, pc))

    def test_tensordot(self):
        na, pa = generate((2,3,4,5))
        nb, pb = generate((4,5,6,7))
        nc = numpy.tensordot(na, nb)
        pc = penne.tensordot(pa, pb)
        self.assertTrue(check(nc, pc))

    def test_inner(self):
        na, pa = generate((2,3,4))
        nb, pb = generate((5,6,4))
        nc = numpy.inner(na, nb)
        pc = penne.inner(pa, pb)
        self.assertTrue(check(nc, pc))

cases = [
    IndexingTestCase,
    #ComponentwiseTestCase,
    #ReductionTestCase,
    #ManipulationTestCase,
    #DotTestCase,
]

alltests = unittest.TestSuite([unittest.TestLoader().loadTestsFromTestCase(case) for case in cases])
unittest.TextTestRunner(verbosity=2).run(alltests)
