import unittest
import numpy
import penne
import penne.conv

import argparse
argparser = argparse.ArgumentParser()
argparser.add_argument('--gpu', dest='gpu', action='store_true')
args = argparser.parse_args()

if args.gpu:
    import pygpu
    penne.use_gpu()

class ConvTestCase(unittest.TestCase):
    def test_convolve(self):
        a = penne.parameter(numpy.random.uniform(-1., 1., (16, 16)))
        b = penne.parameter(numpy.random.uniform(-1., 1., (3, 3)))
        c = penne.conv.convolve(a, b, mode='full')
        o = penne.sum(c * penne.constant(numpy.random.uniform(-1., 1., (18, 18))))
        
        penne.check_gradients(o, delta=0.001)

    def test_mean_pool(self):
        na = numpy.random.uniform(-1., 1., (4, 4, 4))
        a = penne.parameter(na)
        b = penne.conv.mean_pool(a, (2, 2, 2))
        o = penne.sum(b * penne.constant(numpy.random.uniform(-1., 1., (2, 2, 2))))
        nb = penne.compute_value(b)
        self.assertEqual(nb.shape, (2, 2, 2))
        for i in range(2):
            for j in range(2):
                for k in range(2):
                    self.assertAlmostEqual(nb[i,j,k], numpy.mean(na[2*i:2*(i+1),2*j:2*(j+1),2*k:2*(k+1)]))
        penne.check_gradients(o)

        b = penne.conv.mean_pool(a, (2, 2))
        o = penne.sum(b * penne.constant(numpy.random.uniform(-1., 1., (4, 2, 2))))
        nb = penne.compute_value(b)
        self.assertEqual(nb.shape, (4, 2, 2))
        for i in range(4):
            for j in range(2):
                for k in range(2):
                    self.assertAlmostEqual(nb[i,j,k], numpy.mean(na[i,2*j:2*(j+1),2*k:2*(k+1)]))
        penne.check_gradients(o)

cases = [
    ConvTestCase,
]

alltests = unittest.TestSuite([unittest.TestLoader().loadTestsFromTestCase(case) for case in cases])
unittest.TextTestRunner(verbosity=2).run(alltests)
