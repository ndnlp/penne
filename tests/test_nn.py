import unittest
import numpy
import penne

import argparse
argparser = argparse.ArgumentParser()
argparser.add_argument('--gpu', dest='gpu', action='store_true')
args = argparser.parse_args()

if args.gpu:
    import pygpu
    penne.use_gpu()

class SoftmaxTestCase(unittest.TestCase):
    def test_softmax(self):
        x = penne.parameter(numpy.random.uniform(-1., 1., (10,)))
        o = penne.softmax(x)[0]
        penne.check_gradients(o)
        
cases = [
    SoftmaxTestCase,
]

alltests = unittest.TestSuite([unittest.TestLoader().loadTestsFromTestCase(case) for case in cases])
unittest.TextTestRunner(verbosity=2).run(alltests)
