from __future__ import division
import unittest
import numpy
from penne import backend
import time

import argparse
argparser = argparse.ArgumentParser()
argparser.add_argument('--gpu', dest='gpu', action='store_true')
args = argparser.parse_args()

if args.gpu:
    import pygpu
    backend.use_gpu()

def generate(shape, dtype=numpy.float):
    na = numpy.random.uniform(1., 2., shape).astype(dtype)
    ga = backend.array(na)
    return na, ga

class MyTestCase(unittest.TestCase):
    def assertAllClose(self, a, b, msg=None):
        from unittest.util import safe_repr
        if not numpy.allclose(a, b):
            msg = self._formatMessage(msg, "%s is not close to %s" % (safe_repr(a), safe_repr(b)))
            raise self.failureException(msg)

class CreationTestCase(MyTestCase):
    def test_arange(self):
        for n in [10, 5, 100]:
            na = numpy.arange(n)
            ga = backend.arange(n)
            self.assertAllClose(na, ga)

    def test_one_hot(self):
        for n in [10, 5, 100]:
            for i in [0, -1, 2]:
                na = numpy.zeros((n,))
                if 0 <= i < n: na[i] = 1.
                ga = backend.one_hot(n, i)
                self.assertAllClose(na, ga)

class ManipulationTestCase(MyTestCase):
    def test_stack(self):
        na, ga = generate((2,3), numpy.float32)
        nb, gb = generate((2,3), numpy.float32)

        for axis in [0, 1, 2]:
            self.assertAllClose(numpy.stack([na, nb], axis=axis), backend.stack([ga, gb], axis=axis))

class UnaryTestCase(MyTestCase):
    def _test(self, nf, gf):
        for dtype1 in [numpy.float32, numpy.int32]:
            na, ga = generate((2,3), dtype1)
            self.assertAllClose(nf(na), gf(ga))

            for dtype2 in [numpy.float32]:
                nb, gb = generate((2,3), dtype2)
                self.assertAllClose(nf(na, out=nb), 
                                    gf(ga, out=gb))

    def test_neg(self): self._test(numpy.negative, backend.negative)
    def test_abs(self): self._test(numpy.absolute, backend.absolute)

class BinaryTestCase(MyTestCase):
    def _test(self, nf, gf):
        for dtype1 in [numpy.float32]:
            for dtype2 in [numpy.float32, numpy.int32]:
                na, ga = generate((2,3), dtype1)
                nb, gb = generate((2,3), dtype2)
                self.assertAllClose(nf(na,nb), gf(ga,gb))
                for dtype3 in [numpy.float32]:
                    nc, gc = generate((2,3), dtype3)
                    self.assertAllClose(nf(na, nb, out=nc),
                                        gf(ga, gb, out=gc))

    def test_add(self):      self._test(numpy.add, backend.add)
    def test_subtract(self): self._test(numpy.subtract, backend.subtract)
    def test_multiply(self): self._test(numpy.multiply, backend.multiply)
    def test_divide(self):   self._test(numpy.divide, backend.divide)
    def test_power(self):    self._test(numpy.power, backend.power)


class LogicalTestCase(MyTestCase):
    def test_compare(self):
        dtype = numpy.float32
        na, ga = generate((2,3), dtype)
        nb, gb = generate((2,3), dtype)
        self.assertAllClose(na < nb, ga < gb)
        self.assertAllClose(na <= nb, ga <= gb)
        self.assertAllClose(na > nb, ga > gb)
        self.assertAllClose(na >= nb, ga >= gb)

    def test_operators(self):
        dtype = numpy.float32
        na, ga = generate((2,3), dtype)
        nb, gb = generate((2,3), dtype)
        nc, gc = generate((2,3), dtype)
        self.assertAllClose(numpy.logical_not(na<nb), backend.logical_not(ga<gb))
        self.assertAllClose(numpy.logical_and(na<nb, nb<nc), backend.logical_and(ga<gb, gb<gc))
        self.assertAllClose(numpy.logical_or(na<nb, nb<nc), backend.logical_or(ga<gb, gb<gc))

        self.assertAllClose(numpy.where(na<nb, na, nb), backend.where(ga<gb, ga, gb))
        
class ReductionTestCase(MyTestCase):
    def _test(self, nf, gf):
        for dtype in [numpy.float32]: # numpy.int32
            na, ga = generate((2,3,4), dtype)

            for axis in [None, 0, (0,2)]:
                self.assertAllClose(nf(na, axis=axis), gf(ga, axis=axis))
            self.assertAllClose(nf(na, axis=0, keepdims=True), gf(ga, axis=0, keepdims=True))

            nb, gb = generate((1,3,4), dtype)
            self.assertAllClose(nf(na, axis=0, keepdims=True, out=nb), gf(ga, axis=0, keepdims=True, out=gb))


    def test_sum(self):  self._test(numpy.sum, backend.sum)
    def test_amax(self): self._test(numpy.amax, backend.amax)
    def test_amin(self): self._test(numpy.amin, backend.amin)
    def test_mean(self): self._test(numpy.mean, backend.mean)

    def test_argmax(self):
        for dtype in [numpy.float32]: # numpy.int32
            na, ga = generate((2,3,4), dtype)
            for axis in [None, 0, 1, 2]:
                self.assertAllClose(numpy.argmax(na, axis), backend.argmax(ga, axis))

    def test_vecdot(self):
        dtype = numpy.float32
        shape = (2,3,4)
        na, ga = generate(shape, dtype)
        nb, gb = generate(shape, dtype)
        #self.assertAllClose(numpy.sum(na*nb), backend.vecdot(ga, gb))
        for axis in [0,1,2,(0,2)]:
            self.assertAllClose(numpy.sum(na*nb, axis=axis), backend.vecdot(ga, gb, axis=axis))
        for axis in [0,1,2,(0,2)]:
            self.assertAllClose(numpy.sum(na*nb, axis=axis, keepdims=True), backend.vecdot(ga, gb, axis=axis, keepdims=True))

class LinearAlgebraTestCase(MyTestCase):
    def test_dot(self):
        dtype = numpy.float32
        for nd1 in [1,2]:
            for nd2 in [1,2]:
                na, ga = generate((3,)*nd1, dtype)
                nb, gb = generate((3,)*nd2, dtype)
                nc, gc = generate((3,)*(nd1+nd2-2), dtype)
                self.assertAllClose(numpy.dot(na, nb), backend.dot(ga, gb))
                numpy.dot(na, nb, out=nc)
                backend.dot(ga, gb, out=gc)
                self.assertAllClose(nc, gc)
                nc += numpy.dot(na, nb)
                gc = backend.add_dot(ga, gb, gc)
                self.assertAllClose(nc, gc)

                self.assertAllClose(numpy.dot(na, nb.T), backend.dot(ga, gb.T))
                self.assertAllClose(numpy.dot(na.T, nb), backend.dot(ga.T, gb))
                self.assertAllClose(numpy.dot(na.T, nb.T), backend.dot(ga.T, gb.T))

    def test_outer(self):
        dtype = numpy.float32
        na, ga = generate((3,), dtype)
        nb, gb = generate((3,), dtype)
        nc, gc = generate((3,3), dtype)
        self.assertAllClose(numpy.outer(na, nb), backend.outer(ga, gb))
        self.assertAllClose(numpy.outer(na, nb, out=nc), backend.outer(ga, gb, out=gc))
        numpy.outer(na, nb, out=nc.T)
        backend.outer(ga, gb, out=gc.T)
        self.assertAllClose(nc, gc)
        nc += numpy.outer(na, nb)
        gc = backend.add_outer(ga, gb, gc)
        self.assertAllClose(nc, gc)

cases = [
    CreationTestCase,
    ManipulationTestCase,
    UnaryTestCase,
    BinaryTestCase,
    LogicalTestCase,
    ReductionTestCase,
    LinearAlgebraTestCase,
]

alltests = unittest.TestSuite([unittest.TestLoader().loadTestsFromTestCase(case) for case in cases])
unittest.TextTestRunner(verbosity=2).run(alltests)
