import os.path
import subprocess
import collections
import operator
import inspect
from . import expr
from . import calltree

def escape(s):
    return str(s).replace('\\', '\\\\').replace('"', '\\"')

def repr_png(self):
    """IPython magic: show PNG representation of the transducer.
    Adapted from pyfst."""
    process = subprocess.Popen(['dot', '-Tpng'], 
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE, 
                               stderr=subprocess.PIPE)
    out, err = process.communicate(computation_graph(self))
    if err:
        raise Exception(err)
    return out

expr.Expression._repr_png_ = classmethod(repr_png)

node_style = 'node [shape=box,margin=0.1,width=0,height=0,style=filled,fillcolor=lightgrey,penwidth=0,fontname="Courier",fontsize=10];'
edge_style = 'edge [arrowsize=0.5,fontname="Courier",fontsize=8];'
subgraph_style = 'graph [fontname="Courier"; fontsize=8; labelloc="b"; labeljust="l"; color="lightgrey"];' # how come "b" means top

def call_graph(x, merge=False):
    """Draw the call graph of an expression in the DOT language.

    :param merge: Whether to merge different calls to the same function/expression.
    :type merge: bool
    :return: DOT representation of computation graph
    :rtype: str

    The output can be processed using GraphViz's dot command.
    """

    def nid(n):
        if merge:
            return str(n)
        else:
            return id(n)
    
    result = []
    result.append("digraph {")
    result.append('  rankdir=TB;')
    result.append('  '+node_style)
    result.append('  '+edge_style)

    for n in reversed(calltree.bottomup(x)):
        result.append('  "{}" [label="{}"];'.format(nid(n), n))
        if n.parent:
            result.append('  "{}" -> "{}" [label="{}"];'.format(nid(n.parent), nid(n), n.edge_label()))

    result.append('}')
    
    return '\n'.join(result)

def computation_graph(x):
    """Draw the computation graph of an expression in the DOT language.

    :param x: the expression to draw
    :type x: Expression
    :return: DOT representation of computation graph
    :rtype: str

    The output can be processed using GraphViz's dot command.
    """

    result = []
    result.append("digraph {")
    result.append('  rankdir=BT;')
    result.append('  '+node_style)
    result.append('  '+edge_style)
    result.append('  '+subgraph_style)

    # Make call tree top-down
    children = collections.defaultdict(list)
    for n in calltree.bottomup(x):
        children[n.parent].append(n)

    def visit(n, indent, chain):

        # To keep visually simple, print only the top label in a unary chain,
        # unless the chain ends in an Expression, in which case print only
        # the Expression. Most of the time, this is good enough.

        # Expressions don't need line numbers because hopefully the 
        # dependency edges make it clear enough?

        if len(children[n]) == 0:
            result.append(' '*indent + '{} [label="{}"];\n'.format(id(n), n))
            return

        if n is None or n.parent is None:
            subgraph = False
        elif len(children[n]) == 1:
            subgraph = False
            chain = chain + [n]
        else:
            subgraph = True
            chain = chain + [n]

        if subgraph:
            result.append(' '*indent + 'subgraph cluster_{} {{\n'.format(id(n)))
            indent += 2

            n = chain[0]
            if n.parent:
                label = "{}\l(from {})\l".format(n, n.edge_label())
            else:
                label = str(n)
            result.append(' '*indent + 'label="{}";\n'.format(label))
            chain = []

        for c in children[n]:
            visit(c, indent, chain)

        if subgraph:
            result.append(' '*indent + '}\n')

    visit(None, 2, [])

    for subx in expr.topological(x):
        for arg in subx.args:
            result.append("  %s -> %s;\n" % (id(arg.frame), id(subx.frame)))

    result.append("}\n")
    return "".join(result)


