"""Convolution and pooling."""
from __future__ import division
from . import backend
from .expr import *

class convolve(Expression):
    """Discrete convolution. 

    Same as scipy.signal.convolve (n-dimensional) if available;
    otherwise, same as numpy.convolve (1-dimensional)."""

    def __init__(self, x, y, mode='full'):
        self.mode = mode
        Expression.__init__(self, x, y)

    def forward(self):
        x, y = self.args
        self.value = backend.convolve(x.value, y.value, mode=self.mode)

    def backward(self, gradients):
        x, y = self.args
        gz = gradients[self]
        if self.mode != 'full':
            # Since 'valid' and 'same' are equivalent to doing a
            # 'full' convolution followed by clipping, the backward
            # step just needs to first pad the same number of zeros.
            if self.mode == 'valid':
                pad = [n-1 for n in y.shape]
            elif self.mode == 'same':
                pad = [((n-1)//2, n//2) for n in y.shape]
            gz = backend.pad(gz, pad, mode='constant')

        if x.has_parameter:
            gradients[x] += backend.correlate(gz, y.value.conj(), mode='valid')
        if y.has_parameter:
            gradients[y] += backend.correlate(gz, x.value.conj(), mode='valid')

class pool(Expression):
    """Tile an array into nonoverlapping blocks and return an array of
    the blocks.
   
    If blockshape is (a, b, ...), returns result, where

        result[I, i, J, j, ...] = a[I*a+i, J*b+j, ...]

    If blockshape has fewer axes than a does, they are right-aligned.

    :param a:          array to be tiled.
    :type a:           Expression
    :param blockshape: shape of the blocks.
    :type blockshape:  tuple of ints
    """

    def __init__(self, a, blockshape):
        self.blockshape = blockshape
        Expression.__init__(self, a)

    def forward(self):
        vx = self.args[0].value
        if vx.ndim < len(self.blockshape):
            raise ValueError("block has too many axes")
        newshape = []
        blockshape = (None,) * (vx.ndim - len(self.blockshape)) + self.blockshape
        for vd, bd in zip(vx.shape, blockshape):
            if bd is None:
                newshape.append(vd)
            elif vd % bd != 0:
                raise ValueError("block size must divide value size")
            else:
                newshape.extend([vd//bd, bd])
        self.value = vx.reshape(newshape)

    def backward(self, gradients):
        x, = self.args
        if x.has_parameter:
            g = gradients[self]
            g = g.reshape(x.value.shape)
            gradients[x] += g

def max_pool(a, blockshape):
    """Tile an array into nonoverlapping blocks and return the maximum
    value of each block.

    If blockshape has fewer axes than a does, they are right-aligned.

    :param a:          array to be tiled.
    :type a:           Expression
    :param blockshape: shape of the blocks.
    :type blockshape:  tuple of ints
    """

    rk = len(blockshape)
    return amax(pool(a, blockshape), axis=tuple(range(-rk*2+1, 0, 2)))

def mean_pool(a, blockshape):
    """Tile an array into nonoverlapping blocks and return the mean
    value of each block.

    If blockshape has fewer axes than a does, they are right-aligned.

    :param a:          array to be tiled.
    :type a:           Expression
    :param blockshape: shape of the blocks.
    :type blockshape:  tuple of ints
    """

    rk = len(blockshape)
    return mean(pool(a, blockshape), axis=tuple(range(-rk*2+1, 0, 2)))

