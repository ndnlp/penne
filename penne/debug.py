import inspect
import linecache
import traceback
import collections
import os
import sys
import six
import atexit

from .calltree import set_calltree

def file_or_filename(arg):
    if isinstance(arg, six.string_types):
        return open(arg, "w")
    elif hasattr(arg, "write"):
        return arg
    else:
        raise TypeError("a file or filename is required")

### Expression tracebacks

def print_traceback(x):
    sys.stderr.write("Expression traceback (most recent call last):\n")
    framelist = []
    frame = x.frame.parent
    lineno = x.frame.plineno
    while frame is not None:
        code = frame.contents
        linecache.checkcache(code.co_filename)
        text = linecache.getline(code.co_filename, lineno) # need globals?
        framelist.append((code.co_filename, lineno, code.co_name, text))
        frame, lineno = frame.parent, frame.plineno
    sys.stderr.write(''.join(traceback.format_list(reversed(framelist))))

### Tracing

trace = False
trace_file = None

def set_trace(flag=True, file=sys.stderr):
    """Set options for tracing.

    :param flag: turn tracing on/off.
    :type flag: bool
    :param file: file or filename to write tracing output to.
    :type file: file or str
    """
    global trace, trace_file
    if flag:
        trace = True
        trace_file = file_or_filename(file)
    else:
        trace = False
        trace_file = None

### Profiling

profile = False
profile_file = profile_graph_file = None
profile_ncount = collections.Counter()
profile_ntime = collections.Counter()
profile_nmem = collections.Counter()
profile_ecount = collections.Counter()
profile_etime = collections.Counter()
profile_emem = collections.Counter()

def set_profile(flag=True, file=None, graph_file=None):
    """Set options for profiler.

    :param flag: turn profiling on/off.
    :type flag: bool
    :param file: file or filename to write text output to.
    :type file: file or str
    :param graph_file: file or filename to write graph to in DOT format.
    :type graph_file: file or str"""
    global profile, stack, profile_file, profile_graph_file
    if flag:
        profile = True
        if not (file or graph_file):
            raise ValueError("either file or graph_file is required")
        if file:
            profile_file = file_or_filename(file)
            atexit.register(print_profile)
        if graph_file:
            profile_graph_file = file_or_filename(graph_file)
            stack = True
            atexit.register(print_profile_graph)
    else:
        profile = False
        profile_file = profile_graph_file = None

def profile_increment(x, tim, mem):
    if hasattr(x, 'frame'):
        frame = x.frame
        name = str(frame)
        while frame is not None:
            profile_ncount[name] += 1
            profile_ntime[name] += tim
            profile_nmem[name] += mem
            if frame.parent:
                pname = str(frame.parent)
                ename = frame.edge_label() # if a function is called twice on same line, they will be merged
                profile_ecount[pname,ename,name] += 1
                profile_etime[pname,ename,name] += tim
                profile_emem[pname,ename,name] += mem
                name = pname
            frame = frame.parent
    else:
        name = x.__class__.__name__
        profile_ncount[name] += 1
        profile_ntime[name] += tim
        profile_nmem[name] += mem

def print_profile():
    lines = [(value, line) for line, value in profile_ntime.items()]
    lines.sort(reverse=True)

    profile_file.write("{:>8s}  {:>8s}  {:>8s}  {}\n".format("ncalls","time (s)","mem (Mb)","location"))
    for _, line in lines:
        profile_file.write("{:8d}  {:8.2f}  {:8d}  {}\n".format(profile_ncount[line], profile_ntime[line], profile_nmem[line]//1048576, line))

def print_profile_graph():
    from . import viz

    def format_time(t):
        return "{:.2f}".format(t)
    def format_mem(m):
        return m//1048576
    def format_percent(r):
        return int(r*100+0.5)

    profile_graph_file.write("digraph {\n")
    profile_graph_file.write('  rankdir=TB;\n')
    profile_graph_file.write('  '+viz.node_style+'\n')
    profile_graph_file.write('  '+viz.edge_style+'\n')

    for u in profile_ncount:
        profile_graph_file.write('  "{}" [label="{}\\n{}\\n{}"];\n'.format(u, u, format_time(profile_ntime[u]), format_mem(profile_nmem[u])))

    for u, e, v in profile_ecount:
        profile_graph_file.write('  "{}" -> "{}"[label="{}\\n{}\\n{}"];\n'.format(u, v, e, format_time(profile_etime[u,e,v]), format_mem(profile_emem[u,e,v])))

    profile_graph_file.write("}\n")
