import sys
import inspect
import six

"""The call tree is the tree of function calls that built up an
Expression graph.

The root of the tree is None. Its children are the top-level frames
(frames of functions invoked immediately inside the "with
calltracker:" statement) as well as any Expression objects that were
created while the calltracker was not enabled.
"""

class Frame(object):
    __slots__ = ['contents', 'plineno', 'plasti', 'parent']
    """Ersatz frame object that doesn't keep references to local/global variables.

    :type contents:    code object or str
    :param plineno: line number of parent (outer) frame
    :param plasti:  instruction pointer of parent (outer) frame
    """
    def __init__(self, contents, plineno=None, plasti=None, parent=None):
        self.contents = contents
        self.plineno = plineno
        self.plasti = plasti
        self.parent = parent

    def __str__(self):
        return qualname(self.contents)

    def edge_label(self):
        from os.path import basename
        return "{}:{}".format(basename(self.parent.contents.co_filename), self.plineno)

calltree_enabled = False
calltree_stack = None

"""class set_calltree(object):
    def __init__(self, flag=True):
        self.flag = flag
    def __enter__(self):
        global calltree_enabled, calltree_stack
        if self.flag:
            calltree_enabled = True
            calltree_stack = [None, 'dummy']
            sys.setprofile(dispatch)
    def __exit__(self, type, value, traceback):
        if self.flag:
            sys.setprofile(None)
            calltree_enabled = False
            calltree_stack = None"""

def set_calltree(flag=True):
    """Set options for the call tree.

    :param flag: turn call tree on/off.
    :type flag:  bool"""

    global calltree_enabled, calltree_stack
    if flag:
        calltree_enabled = True
        calltree_stack = [None, 'dummy']
        sys.setprofile(dispatch)
    else:
        sys.setprofile(None)
        calltree_enabled = False
        calltree_stack = None

def dispatch(frame, event, arg):
    if event == "call":
        node = Frame(frame.f_code, frame.f_back.f_lineno, frame.f_back.f_lasti, calltree_stack[-1])
        calltree_stack.append(node)
    elif event == "return":
        if len(calltree_stack) > 1:
            calltree_stack.pop()
        else: # auto shutoff
            set_calltree(False)

def bottomup(x):
    from . import expr
    visited = set()
    result = []
    for subx in expr.topological(x):
        if hasattr(subx, 'frame'):
            frame = subx.frame
            while frame is not None and frame not in visited:
                visited.add(frame)
                result.append(frame)
                frame = frame.parent
        else:
            subx.frame = Frame(subx.__class__)
            result.append(subx.frame)
    return result

### From https://github.com/wbolster/qualname. Modified to work on code objects.

"""
License
=======

Copyright (c) 2015, Wouter Bolsterlee

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.

* Neither the name of the author nor the names of its contributors may be used
  to endorse or promote products derived from this software without specific
  prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*(This is the OSI approved 3-clause "New BSD License".)*
"""

import ast

_filename_cache = {}
_object_cache = {}

class _Visitor(ast.NodeVisitor):
    def __init__(self):
        super(_Visitor, self).__init__()
        self.stack = []
        self.qualnames = {}

    def store_qualname(self, lineno):
        qn = ".".join(n for n in self.stack)
        self.qualnames[lineno] = qn

    def visit_FunctionDef(self, node):
        self.stack.append(node.name)
        self.store_qualname(node.lineno)
        self.stack.append('<locals>')
        self.generic_visit(node)
        self.stack.pop()
        self.stack.pop()

    def visit_ClassDef(self, node):
        self.stack.append(node.name)
        self.store_qualname(node.lineno)
        self.generic_visit(node)
        self.stack.pop()


def qualname(obj):
    """Find out the qualified name for a class or function."""

    # For Python 3.3+, this is straight-forward.
    if hasattr(obj, '__qualname__'):
        return obj.__qualname__

    if obj in _object_cache:
        return _object_cache[obj]

    # For older Python versions, things get complicated.
    # Obtain the filename and the line number where the
    # class/method/function is defined.
    try:
        filename = inspect.getsourcefile(obj)
    except TypeError:
        return obj.__qualname__  # raises a sensible error
    if inspect.isclass(obj):
        try:
            _, lineno = inspect.getsourcelines(obj)
        except (OSError, IOError):
            return obj.__qualname__  # raises a sensible error
    elif inspect.isfunction(obj) or inspect.ismethod(obj):
        if hasattr(obj, 'im_func'):
            # Extract function from unbound method (Python 2)
            obj = obj.im_func
        try:
            code = obj.__code__
        except AttributeError:
            code = obj.func_code
        lineno = code.co_firstlineno
    elif inspect.iscode(obj):
        code = obj
        lineno = code.co_firstlineno
    else:
        return obj.__qualname__  # raises a sensible error

    # Re-parse the source file to figure out what the
    # __qualname__ should be by analysing the abstract
    # syntax tree. Use a cache to avoid doing this more
    # than once for the same file.
    qualnames = _filename_cache.get(filename)
    if qualnames is None:
        with open(filename, 'r') as fp:
            source = fp.read()
        node = ast.parse(source, filename)
        visitor = _Visitor()
        visitor.visit(node)
        _filename_cache[filename] = qualnames = visitor.qualnames
    try:
        result = qualnames[lineno]
    except KeyError:
        return obj.__qualname__  # raises a sensible error

    _object_cache[obj] = result
    return result
