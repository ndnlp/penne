"""NumPy-like wrapper around libgpuarray/pygpu.

Much of this is from pygpu's _array.py, elemwise.py, and reduction.py.
"""

import pygpu
import pygpu.tools
import pygpu.blas
import numpy
import six
from six.moves import range

def init_gpu(device="cuda"):
    pygpu.set_default_context(pygpu.init(device))

def memory_usage():
    ctx = pygpu.get_default_context()
    return ctx.total_gmem - ctx.free_gmem

max_bits = 32
def set_max_bits(b):
    global max_bits
    max_bits = b

class ndarray(pygpu.gpuarray.GpuArray):
    def __str__(self):             return str(numpy.asarray(self))
    def __repr__(self):            return repr(numpy.asarray(self))
    def __float__(self):           return float(numpy.asarray(self))
    def __int__(self):             return int(numpy.asarray(self))
    def __reduce__(self):          return (asarray, (numpy.asarray(self),))

    def __pos__(self):             return self.copy()
    def __neg__(self):             return negative(self)
    def __abs__(self):             return absolute(self)

    def __add__(self, other):      return add(self, other)
    def __radd__(self, other):     return add(other, self)
    def __iadd__(self, other):     return add(self, other, out=self)

    def __sub__(self, other):      return subtract(self, other)
    def __rsub__(self, other):     return subtract(other, self)
    def __isub__(self, other):     return subtract(self, other, out=self)

    def __mul__(self, other):      return multiply(self, other)
    def __rmul__(self, other):     return multiply(other, self)
    def __imul__(self, other):     return multiply(self, other, out=self)

    def __div__(self, other):      return divide(self, other)
    def __rdiv__(self, other):     return divide(other, self)
    def __idiv__(self, other):     return divide(self, other, out=self)

    def __truediv__(self, other):  return true_divide(self, other)
    def __rtruediv__(self, other): return true_divide(other, self)
    def __itruediv__(self, other): return true_divide(self, other, out=self)

    def __pow__(self, other):      return power(self, other)
    def __rpow__(self, other):     return power(other, self)
    def __ipow__(self, other):     return power(self, other, out=self)

    def __eq__(self, other):       return equal(self, other)
    def __ne__(self, other):       return not_equal(self, other)
    def __lt__(self, other):       return less(self, other)
    def __le__(self, other):       return less_equal(self, other)
    def __gt__(self, other):       return greater(self, other)
    def __ge__(self, other):       return greater_equal(self, other)

    T = property(pygpu.gpuarray.GpuArray.transpose)

def shape(a):                      return asarray(a).shape
def ndim(a):                       return asarray(a).ndim

### Array creation routines

def _demote_type(dtype):
    assert max_bits == 32
    if dtype in [float, numpy.float64, numpy.float128]:
        return numpy.dtype(numpy.float32)
    if dtype in [int, numpy.int64]:
        return numpy.dtype(numpy.int32)
    return dtype

def result_type(*args):
    dtype = numpy.result_type(*args)
    if max_bits:
        dtype = _demote_type(dtype)
    return dtype

def array(proto, copy=True):
    proto = numpy.asarray(proto)
    if isinstance(proto, numpy.ndarray) and max_bits:
        proto = proto.astype(_demote_type(proto.dtype))
    return pygpu.array(proto, cls=ndarray, copy=copy)

def asarray(proto):
    if isinstance(proto, ndarray):
        return proto
    else:
        return array(proto, copy=False)

def empty(shape, dtype=float):
    if max_bits: dtype = _demote_type(dtype)
    return pygpu.empty(shape, dtype=dtype, cls=ndarray)
def empty_like(a, dtype=None):
    return empty(a.shape, dtype=dtype or a.dtype)

def zeros(shape, dtype=float):
    if max_bits: dtype = _demote_type(dtype)
    return pygpu.zeros(shape, dtype=numpy.dtype(dtype), cls=ndarray)
def zeros_like(a, dtype=None):
    return zeros(a.shape, dtype=dtype or a.dtype)

def full(shape, fill_value, dtype=None):
    if dtype is None: dtype = numpy.array(fill_value).dtype
    if max_bits: dtype = _demote_type(dtype)
    a = pygpu.empty(shape, dtype=dtype, cls=ndarray)
    a[...] = fill_value
    return a
def full_like(a, fill_value, dtype=None):
    return full(a.shape, fill_value, dtype=dtype or a.dtype)

def ones(shape, dtype=float):
    return full(shape, 1, dtype)
def ones_like(a, dtype=None):
    return full_like(a, 1, dtype)

class _Random(object):
    @staticmethod
    def uniform(low, high, size, dtype=float):
        return array(numpy.random.uniform(float(low), float(high), size).astype(dtype))

    @staticmethod
    def normal(loc, scale, size, dtype=float):
        return array(numpy.random.normal(float(loc), float(scale), size).astype(dtype))

random = _Random()

_arange_cache = None
def arange(stop):
    """Same as numpy.arange, but because return values are cached, you should 
    not modify them."""

    global _arange_cache
    if _arange_cache is None:
        _arange_cache = array(numpy.arange(1))
    size = _arange_cache.size
    while stop > size:
        size *= 2
    if size > _arange_cache.size:
        _arange_cache = array(numpy.arange(size))
    return _arange_cache[:stop]

def one_hot(n, idx, axis=-1, dtype=float):
    """Creates a one-hot vector or a batch of one-hot vectors.

    If an index is out of bounds (including if it is negative), then
    the resulting vector is all zeros.

    :param n:     Size of the new axis to create.
    :param idx:   Index or indices to place ones.
    :type idx:    int or array[int]
    :param axis:  Which axis to create.
    :param dtype: Type of elements.

    """
    idx = asarray(idx)
    nd = idx.ndim
    if axis < 0: axis += nd+1
    idx = idx.reshape(idx.shape[:axis] + (1,) + idx.shape[axis:])
    r = arange(n).reshape(               (n,) + (1,)*(nd-axis))
    return elemwise2("{0}=={1}", idx, r, out_dtype=dtype)

### Loading/saving

def save(f, a):
    numpy.save(f, numpy.asarray(a))

def load(f):
    return asarray(numpy.load(f))

### Array manipulation routines

def reshape(a, *params):   return a.reshape(*params)

def ravel(a):              return reshape(a, (-1,))

def transpose(a, *params): return a.transpose(*params)

def swapaxes(a, ax1, ax2):
    axes = list(range(a.ndim))
    axes[ax1], axes[ax2] = axes[ax2], axes[ax1]
    return transpose(a, axes)

def moveaxis(a, src, dst):
    """Like numpy.moveaxis, but only allows single axis."""
    axes = list(range(a.ndim))
    axes.insert(dst, axes.pop(src))
    return transpose(a, axes)

import pygpu.operations

def concatenate(arrays, axis=0):
    return asarray(pygpu.operations.concatenate(arrays, axis))

def stack(arrays, axis=0):
    if not arrays:
        raise ValueError('need at least one array to stack')

    shapes = set(arr.shape for arr in arrays)
    if len(shapes) != 1:
        raise ValueError('all input arrays must have the same shape')

    result_ndim = arrays[0].ndim + 1
    if not -result_ndim <= axis < result_ndim:
        msg = 'axis {0} out of bounds [-{1}, {1})'.format(axis, result_ndim)
        raise IndexError(msg)
    if axis < 0:
        axis += result_ndim

    shape = arrays[0].shape[:axis] + (1,) + arrays[0].shape[axis:]
    expanded_arrays = [arr.reshape(shape) for arr in arrays]
    return concatenate(expanded_arrays, axis=axis)

### Elementwise operations

## Unary

@pygpu.tools.lfu_cache(1000)
def get_elemwise1(context, op, adtype, odtype):
    """Returns a unary elementwise kernel.

    context: GpuContext
    op: str, a C expression written in terms of {0}
    adtype: argument dtype
    odtype: output dtype
    """
    a_arg = pygpu.elemwise.arg('a', adtype, read=True)
    res_arg = pygpu.elemwise.arg('res', odtype, write=True)
    expr = "res = " + op.format('a')
    return pygpu.elemwise.GpuElemwise(context, expr, [a_arg, res_arg])

def elemwise1(op, a, out=None, out_dtype=None):
    a = asarray(a)
    if out is None:
        out = a._empty_like_me(dtype=out_dtype)
    if out.shape != a.shape:
        raise ValueError("out argument has wrong shape (was {}, expected {})".format(out.shape, out_shape))
    k = get_elemwise1(a.context, op, a.dtype, out.dtype)
    k(a, out)
    return out

def absolute(a, out=None):
    if a.dtype.kind == 'u':
        return a.copy()
    elif a.dtype.kind == 'f':
        op = "fabs({0})"
    elif a.dtype.itemsize < 4:
        # cuda 5.5 finds the c++ stdlib definition if we don't cast here.
        op = "abs((int){0})"
    else:
        op = "abs({0})"
    return elemwise1(op, a, out)
def fabs(a, out=None):             return elemwise1("fabs({0})",   a, out)

def negative(a, out=None):         return elemwise1("-{0}",       a, out)

def log(a, out=None):              return elemwise1("log({0})",   a, out)
def log2(a, out=None):             return elemwise1("log2({0})",  a, out)
def log10(a, out=None):            return elemwise1("log10({0})", a, out)
def log1p(a, out=None):            return elemwise1("log1p({0})", a, out)
def exp(a, out=None):              return elemwise1("exp({0})",   a, out)
def exp2(a, out=None):             return elemwise1("exp2({0})",  a, out)
def exp10(a, out=None):            return elemwise1("exp10({0})", a, out)
def expm1(a, out=None):            return elemwise1("expm1({0})", a, out)
def sqrt(a, out=None):             return elemwise1("sqrt({0})",  a, out)
def cbrt(a, out=None):             return elemwise1("cbrt({0})",  a, out)

def sin(a, out=None):              return elemwise1("sin({0})",   a, out)
def cos(a, out=None):              return elemwise1("cos({0})",   a, out)
def tan(a, out=None):              return elemwise1("tan({0})",   a, out)
def arcsin(a, out=None):           return elemwise1("asin({0})",  a, out)
def arccos(a, out=None):           return elemwise1("acos({0})",  a, out)
def arctan(a, out=None):           return elemwise1("atan({0})",  a, out)
def sinh(a, out=None):             return elemwise1("sinh({0})",  a, out)
def cosh(a, out=None):             return elemwise1("cosh({0})",  a, out)
def tanh(a, out=None):             return elemwise1("tanh({0})",  a, out)
def arcsinh(a, out=None):          return elemwise1("asinh({0})", a, out)
def arccosh(a, out=None):          return elemwise1("acosh({0})", a, out)
def arctanh(a, out=None):          return elemwise1("atanh({0})", a, out)

# fused kernels to speed up sigmoid and tanh
def sigmoid(a, out=None):          return elemwise1("1.0/(1.0+exp(-{0}))", a, out)
def xonemx(a, out=None):           return elemwise1("{0}*(1.0-{0})", a, out)
def onemxx(a, out=None):           return elemwise1("1.0-{0}*{0}", a, out)

## Binary

def _align(args):
    nd = 0
    context = cls = None
    for i, arg in enumerate(args):
        if isinstance(arg, pygpu.gpuarray.GpuArray):
            if context is None:
                context = arg.context
                cls = arg.__class__
            else:
                assert arg.context == context
        else:
            args[i] = asarray(arg)
        nd = _max(nd, args[i].ndim)

    shp = [0]*nd

    for i, arg in enumerate(args):
        if isinstance(arg, pygpu.gpuarray.GpuArray):
            ashp = (1,)*(nd-arg.ndim) + arg.shape
            args[i] = args[i].reshape(ashp)
            for j in range(nd):
                shp[j] = _max(shp[j], ashp[j])
            
    return args, (tuple(shp), result_type(*args), context, cls)

@pygpu.tools.lfu_cache(1000)
def get_elemwise2(context, op, adtype, bdtype, odtype):
    """Returns a binary elementwise kernel.

    context: GpuContext
    op: str, a C expression written in terms of {0} and {1}
    adtype, bdtype: argument dtypes
    odtype: output dtype
    """

    a_arg = pygpu.elemwise.arg('a', adtype, read=True)
    b_arg = pygpu.elemwise.arg('b', bdtype, read=True)
    res_arg = pygpu.elemwise.arg('res', odtype, write=True)
    expr = "res = " + op.format('a', 'b')
    return pygpu.elemwise.GpuElemwise(context, expr, [a_arg, b_arg, res_arg])

@pygpu.tools.lfu_cache(1000)
def get_ielemwise2(context, op, adtype, bdtype):
    a_arg = pygpu.elemwise.arg('a', adtype, read=True)
    b_arg = pygpu.elemwise.arg('b', bdtype, read=True)
    expr = "a = " + op.format('a', 'b')
    return pygpu.elemwise.GpuElemwise(context, expr, [a_arg, b_arg])

def elemwise2(op, a, b, out=None, out_dtype=None):
    (a, b), (out_shape, out_dtype1, context, cls) = _align([a, b])
    out_dtype = out_dtype or out_dtype1

    if out is None:
        out = pygpu.gpuarray.empty(out_shape, out_dtype, context=context, cls=cls)
    if out.shape != out_shape:
        raise ValueError("out argument has wrong shape (was {}, expected {})".format(out.shape, out_shape))

    if out is a:
        k = get_ielemwise2(context, op, a.dtype, b.dtype)
        k(a, b, broadcast=True)
    else:
        k = get_elemwise2(context, op, a.dtype, b.dtype, out.dtype)
        k(a, b, out, broadcast=True)
    return out

def add(a, b, out=None):           return elemwise2("{0}+{1}", a, b, out)
def subtract(a, b, out=None):      return elemwise2("{0}-{1}", a, b, out)
def multiply(a, b, out=None):      return elemwise2("{0}*{1}", a, b, out)

def divide(a, b, out=None):
    if six.PY2 and result_type(a, b).kind in ['i', 'u']:
        raise NotImplementedError('Floor division not implemented')
    else:
        return true_divide(a, b, out)
def true_divide(a, b, out=None):
    return elemwise2("{0}/{1}", a, b, out)
    
def power(a, b, out=None):
    if ndim(b) == 0 and b == 2:
        return elemwise1("{0}*{0}", a, out)
    return elemwise2("pow({0},{1})", a, b, out)

# to do: x and y are different int types

def maximum(a, b, out=None):       
    if result_type(a, b).kind in 'ui':
        return elemwise2("max({0},{1})", a, b, out)
    else:
        return elemwise2("fmax({0},{1})", a, b, out)
def minimum(a, b, out=None):
    if result_type(a, b).kind in 'ui':
        return elemwise2("min({0},{1})", a, b, out)
    else:
        return elemwise2("fmin({0},{1})", a, b, out)

def arctan2(a, b, out=None):       return elemwise2("atan2({0},{1})", a, b, out)
def hypot(a, b, out=None):         return elemwise2("hypot({0},{1})", a, b, out)

### Logical operations

def equal(a, b, out=None):         return elemwise2("{0} == {1}", a, b, out=out, out_dtype=numpy.bool)
def not_equal(a, b, out=None):     return elemwise2("{0} != {1}", a, b, out=out, out_dtype=numpy.bool)
def greater(a, b, out=None):       return elemwise2("{0} > {1}", a, b, out=out, out_dtype=numpy.bool)
def greater_equal(a, b, out=None): return elemwise2("{0} >= {1}", a, b, out=out, out_dtype=numpy.bool)
def less(a, b, out=None):          return elemwise2("{0} < {1}", a, b, out=out, out_dtype=numpy.bool)
def less_equal(a, b, out=None):    return elemwise2("{0} <= {1}", a, b, out=out, out_dtype=numpy.bool)
def logical_and(a, b, out=None):   return elemwise2("{0} && {1}", a, b, out=out, out_dtype=numpy.bool)
def logical_or(a, b, out=None):    return elemwise2("{0} || {1}", a, b, out=out, out_dtype=numpy.bool)
def logical_not(a, out=None):      return elemwise1("!{0}", a, out=out, out_dtype=numpy.bool)

@pygpu.tools.lfu_cache(1000)
def get_elemwise3(context, op, adtype, bdtype, cdtype, odtype):
    """Returns a ternary elementwise kernel.

    context: 
    op: str, a C expression written in terms of {0}, {1}, and {2}
    adtype, bdtype, cdtype: argument dtypes
    odtype: output dtype
    """

    expr = "res = " + op.format('a', 'b', 'c')
    a_arg = pygpu.elemwise.arg('a', adtype, read=True)
    b_arg = pygpu.elemwise.arg('b', bdtype, read=True)
    c_arg = pygpu.elemwise.arg('c', cdtype, read=True)
    res_arg = pygpu.elemwise.arg('res', odtype, write=True)
    return pygpu.elemwise.GpuElemwise(context, expr, [a_arg, b_arg, c_arg, res_arg])

def elemwise3(op, a, b, c, out=None, out_dtype=None):
    (a, b, c), (out_shape, out_dtype1, context, cls) = _align([a, b, c])
    out_dtype = out_dtype or out_dtype1

    if out is None:
        out = pygpu.gpuarray.empty(out_shape, out_dtype, context=context, cls=cls)
    if out.shape != out_shape:
        raise ValueError("out argument has wrong shape (was {}, expected {})".format(out.shape, out_shape))

    k = get_elemwise3(context, op, a.dtype, b.dtype, c.dtype, out.dtype)
    k(a, b, c, out, broadcast=True)
    return out

def where(cond, a, b, out=None):
    return elemwise3("{0} ? {1} : {2}", cond, a, b, out=out, out_dtype=result_type(a, b))

### Reductions

def _redux(nd, axis):
    if axis is None:
        redux = [True] * nd
    else:
        redux = [False] * nd

        if not isinstance(axis, (list, tuple)):
            axis = (axis,)

        for ax in axis:
            if ax < 0:
                ax += nd
            if ax < 0 or ax >= nd:
                raise ValueError('axis out of bounds')
            redux[ax] = True
    return redux

@pygpu.tools.lfu_cache(1000)
def get_reduce1(context, op, a_arg, neutral, redux, odtype):
    reduce_expr = op.format("a", "b")
    return pygpu.reduction.ReductionKernel(context, dtype_out=odtype, neutral=neutral,
                                           reduce_expr=reduce_expr, redux=redux,
                                           arguments=[a_arg])

def reduce1(op, a, neutral, axis=None, keepdims=False, out=None):
    a = asarray(a)
    if a.ndim == 0:
        assert axis is None
        a = a.reshape((1,))
    redux = tuple(_redux(a.ndim, axis))
    shape0 = tuple(d for i, d in enumerate(a.shape) if not redux[i]) # keepdims == False
    shape1 = tuple(d if not redux[i] else 1 for i, d in enumerate(a.shape)) # keepdims == True
    out_shape = shape1 if keepdims else shape0
    out_dtype = out.dtype if out is not None else a.dtype

    k = get_reduce1(a.context, 
                    op, 
                    pygpu.tools.as_argument(a, 'a'), 
                    neutral, 
                    redux,
                    out_dtype)

    if out is None:
        out = pygpu.gpuarray.empty(shape0, a.dtype, context=a.context, cls=a.__class__)
    else:
        if out.shape != out_shape:
            raise TypeError("output array has wrong shape")

    try:
        out.shape = shape0
        k(a, out=out)
    finally:
        out.shape = out_shape

    return out

def asum(a, axis=None, keepdims=False, out=None):
    return reduce1("{0}+{1}", a, 0., axis, keepdims, out)
sum = asum

def amax(a, axis=None, keepdims=False, out=None):
    if issubclass(a.dtype.type, numpy.integer):
        return reduce1("max({0},{1})", a, numpy.iinfo(a.dtype).min, axis, keepdims, out)
    else:
        return reduce1("fmax({0},{1})", a, "-1.0/0.0", axis, keepdims, out)
_max = max
max = amax

def amin(a, axis=None, keepdims=False, out=None):
    if issubclass(a.dtype.type, numpy.integer):
        return reduce1("min({0},{1})", a, numpy.iinfo(a.dtype).max, axis, keepdims, out)
    else:
        return reduce1("fmin({0},{1})", a, "1.0/0.0", axis, keepdims, out)
min = amin

# Unfortunately argmax and argmin are slow

def argmax(a, axis=None, out=None):
    if axis is None:
        n = a.size
        r = arange(n).reshape(a.shape)
    else:
        if axis < 0: axis += a.ndim
        n = a.shape[axis]
        r = arange(n).reshape((n,)+(1,)*(a.ndim-axis-1))
    return reduce3("{{0}}=={{1}}?{{2}}:{n}".format(n=n), "min({0},{1})", a, amax(a, axis, keepdims=True), r, n, axis, False, out, odtype=r.dtype)

def argmin(a, axis=None, out=None):
    if axis is None:
        n = a.size
        r = arange(n).reshape(a.shape)
    else:
        if axis < 0: axis += a.ndim
        n = a.shape[axis]
        r = arange(n).reshape((n,)+(1,)*(a.ndim-axis-1))
    return reduce3("{{0}}=={{1}}?{{2}}:{n}".format(n=n), "min({0},{1})", a, amin(a, axis, keepdims=True), r, n, axis, False, out, odtype=r.dtype)

# to do: do division first as map
def mean(a, axis=None, keepdims=False, out=None):
    a = asarray(a)
    # numpy.mean casts integral types to float64
    if issubclass(a.dtype.type, (numpy.integer, numpy.bool_)) and out is None:
        a = a.astype(numpy.float64)
    out = sum(a, axis, keepdims, out)
    out /= a.size/out.size
    return out

@pygpu.tools.lfu_cache(1000)
def get_reduce2(context, map_op, reduce_op, a_arg, b_arg, neutral, redux, odtype):
    map_expr = map_op.format(a_arg.expr(), b_arg.expr())
    reduce_expr = reduce_op.format("a", "b")
    if odtype is None:
        odtype = result_type(a_arg.dtype, b_arg.dtype)

    return pygpu.reduction.ReductionKernel(context, dtype_out=odtype, neutral=neutral,
                                           map_expr=map_expr,
                                           reduce_expr=reduce_expr, redux=redux,
                                           arguments=[a_arg, b_arg])

def reduce2(map_op, reduce_op, a, b, neutral, axis=None, keepdims=False, out=None):
    (a, b), (align_shape, out_dtype, context, cls) = _align([a, b])
    redux = tuple(_redux(len(align_shape), axis))

    shape0 = tuple(d for i, d in enumerate(align_shape) if not redux[i]) # keepdims == False
    shape1 = tuple(d if not redux[i] else 1 for i, d in enumerate(align_shape)) # keepdims == True
    out_shape = shape1 if keepdims else shape0
    if out is not None: out_dtype = out.dtype

    k = get_reduce2(context, 
                    map_op, reduce_op,
                    pygpu.tools.as_argument(a, 'a'), 
                    pygpu.tools.as_argument(b, 'b'), 
                    neutral,
                    redux,
                    out_dtype)

    if out is None:
        out = pygpu.gpuarray.empty(shape0, out_dtype, context=context, cls=cls)
    else:
        if out.shape != out_shape:
            raise TypeError("output array has wrong shape")

    try:
        out.shape = shape0
        k(a, b, out=out, broadcast=True)
    finally:
        out.shape = out_shape

    return out

@pygpu.tools.lfu_cache(1000)
def get_reduce3(context, map_op, reduce_op, a_arg, b_arg, c_arg, neutral, redux, odtype):
    map_expr = map_op.format(a_arg.expr(), b_arg.expr(), c_arg.expr())
    reduce_expr = reduce_op.format("a", "b", "c")
    if odtype is None:
        odtype = result_type(a_arg.dtype, b_arg.dtype, c_arg.dtype)
    return pygpu.reduction.ReductionKernel(context, dtype_out=odtype, neutral=neutral,
                                           map_expr=map_expr,
                                           reduce_expr=reduce_expr, redux=redux,
                                           arguments=[a_arg, b_arg, c_arg])

def reduce3(map_op, reduce_op, a, b, c, neutral, axis=None, keepdims=False, out=None, odtype=None):

    (a, b, c), (align_shape, out_dtype, context, cls) = _align([a, b, c])
    redux = tuple(_redux(len(align_shape), axis))

    shape0 = tuple(d for i, d in enumerate(align_shape) if not redux[i]) # keepdims == False
    shape1 = tuple(d if not redux[i] else 1 for i, d in enumerate(align_shape)) # keepdims == True
    out_shape = shape1 if keepdims else shape0
    if odtype is not None: out_dtype = odtype
    if out is not None: out_dtype = out.dtype

    k = get_reduce3(context, 
                    map_op, reduce_op,
                    pygpu.tools.as_argument(a, 'a'), 
                    pygpu.tools.as_argument(b, 'b'), 
                    pygpu.tools.as_argument(c, 'c'), 
                    neutral,
                    redux,
                    out_dtype)

    if out is None:
        out = pygpu.gpuarray.empty(shape0, out_dtype, context=context, cls=cls)
    else:
        if out.shape != out_shape:
            raise TypeError("output array has wrong shape")

    try:
        out.shape = shape0
        k(a, b, c, out=out, broadcast=True)
    finally:
        out.shape = out_shape

    return out

### Linear algebra

def _dot(a, b, beta, out=None):
    if not 1 <= a.ndim <= 2:
        raise ValueError('more than 2 axes not supported')
    if b.ndim == 1:
        out_shape = a.shape[:-1]
    elif b.ndim == 2:
        out_shape = a.shape[:-1] + (b.shape[-1],)
    else:
        raise ValueError('more than 2 axes not supported')

    out_dtype = result_type(a, b)
    if a.dtype != out_dtype:
        a = a.astype(out_dtype)
    if b.dtype != out_dtype:
        b = b.astype(out_dtype)

    if out is None:
        out = empty(out_shape, out_dtype)
    elif out.dtype != out_dtype:
        raise TypeError("output array is not of correct dtype")

    if a.ndim == 1:
        if b.ndim == 1:
            out[...] = beta*out + reduce2("{0}*{1}", "{0}+{1}", a, b, 0., None, False)
        else:
            pygpu.blas.gemv(1., b, a, beta, out, trans_a=True, overwrite_y=True)
    else:
        if b.ndim == 1:
            pygpu.blas.gemv(1., a, b, beta, out, overwrite_y=True)
        else:
            pygpu.blas.gemm(1., a, b, beta, out, overwrite_c=True)

    return out

def dot(a, b, out=None):
    """Only supports arrays with up to 2 axes.""" # because that is all that expr.dot needs
    if ndim(a) == 0 or ndim(b) == 0:
        return multiply(a, b, out)
    else:
        return asarray(_dot(a, b, 0., out))

def add_dot(a, b, out):
    if ndim(a) == 0 or ndim(b) == 0:
        out += a * b
        return out
    elif ndim(out) == 0:
        return out + dot(a, b) # to do: check for out == 0
    else:
        return _dot(a, b, 1., out)

def outer(a, b, out=None):
    out_dtype = result_type(a, b)
    if a.dtype != out_dtype:
        a = a.astype(out_dtype)
    if b.dtype != out_dtype:
        b = b.astype(out_dtype)
    if out is not None and out.dtype != out_dtype:
        raise TypeError("output array is not of correct dtype")

    if out is None:
        return asarray(pygpu.blas.ger(1., a, b)) # ger returns GpuArray
    else:
        out[...] = 0.
        pygpu.blas.ger(1., a, b, out, overwrite_a=True)
        return out

def add_outer(a, b, out):
    if ndim(out) == 0:
        return out + outer(a, b)

    out_dtype = result_type(a, b)
    if a.dtype != out_dtype:
        a = a.astype(out_dtype)
    if b.dtype != out_dtype:
        b = b.astype(out_dtype)
    if out is not None and out.dtype != out_dtype:
        raise TypeError("output array is not of correct dtype")

    pygpu.blas.ger(1., a, b, out, overwrite_a=True)
    return out

def vecdot(a, b, axis=None, keepdims=False, out=None):
    return reduce2("{0}*{1}", "{0}+{1}", a, b, 0., axis, keepdims, out)

from backend_cusparse import *
