"""Recurrent neural networks as finite-state transducers."""

from six.moves import range, zip
import numpy
from .expr import *
from .nn import *

def _get_mask(kwargs):
    """Because Python 2 doesn't allow def step(self, *inp, mask=True)"""
    mask = kwargs.pop('mask', True)
    if kwargs: raise TypeError('unexpected keyword argument')
    return mask

class Transducer(object):
    """Base class for transducers."""

    def state(self):
        """Return the transducer's current state."""
        return None

    def start(self, state=None):
        """Prepare the transducer to read a new sequence.
        :param state: initial state
        :type state:  Expression
        """
        pass

    def step(self, *inp, **kwargs):
        pass

    def transduce(self, inps, mask=None, reverse=False):
        """Apply transducer to a sequence of input symbols.
        :param inps:    list of input symbols
        :param mask:    mask for input symbols
        :param reverse: reverse direction
        """

        if isinstance(inps, Expression):
            raise TypeError("can only transduce sequences of Expressions (not Expressions")
        if mask is None:
            mask = [True]*len(inps)
        self.start()
        outputs = []
        if reverse:
            for inp, m in reversed(list(zip(inps, mask))):
                outputs.append(self.step(inp, mask=m))
            outputs.reverse()
        else:
            for inp, m in zip(inps, mask):
                outputs.append(self.step(inp, mask=m))
        return outputs

class Map(Transducer):
    """Stateless transducer that just applies a function to every symbol.

    :param f:  function to apply to every symbol
    """

    def __init__(self, f):
        self.f = f

    def step(self, inp, mask=True):
        return self.f(inp)

    def transduce(self, inps, mask=None, reverse=False):
        if isinstance(inps[0], Expression):
            inps = stack(inps)
        else:
            inps = numpy.stack(inps)
        out = self.f(inps)
        return list(out)

class Stack(Transducer):
    """A stack of recurrent networks, or, the composition of FSTs.

    :param layers: recurrent networks to stack
    :type layers:  list of Transducers
    """

    def __init__(self, *layers):
        self.layers = layers

    def state(self):
        return [layer.state() for layer in self.layers]

    def start(self, state=None):
        if state is None:
            for layer in self.layers:
                layer.start()
        else:
            if len(state) != len(self.layers):
                raise ValueError("Stack started with wrong number of states")
            for layer, lstate in zip(self.layers, state):
                layer.start(lstate)

    def step(self, *inp, **kwargs):
        mask = _get_mask(kwargs)
        # okay for first layer to have multiple inputs
        v = self.layers[0].step(*inp, mask=mask)
        for layer in self.layers[1:]:
            v = layer.step(v, mask=mask)
        return v

    def transduce(self, inps, mask=None, reverse=False):
        vs = inps
        for layer in self.layers:
            vs = layer.transduce(vs, mask, reverse)
        return vs

class Simple(Transducer):
    """Simple (Elman) recurrent network.

    :param insize:  number of input units.
    :param outsize: number of output units.
    :param f:       activation function (default tanh)
    """

    def __init__(self, insize, outsize, f=tanh):
        dims = [insize, outsize]
        self.layer = Layer(dims, outsize, f=f)
        self.h0 = zeros((outsize,)) # or parameter?

    def state(self):
        return self.h
        
    def start(self, state=None):
        if state is None:
            state = self.h0
        self.h = state

    def step(self, inp, **kwargs):
        """inp can be either a vector Expression or an int """
        mask = _get_mask(kwargs)
        h = self.layer(inp, self.h)
        self.h = where(mask, h, self.h)
        return self.h

class GatedRecurrentUnit(Transducer):
    """Gated recurrent unit.

    Cho et al., 2014. Learning phrase representations using RNN
    encoder-decoder for statistical machine translation. In
    Proc. EMNLP.

    :param insize:  size of input vector, or list of sizes of input
                    vectors
    :param outsize: size of output vector
    """

    def __init__(self, insize, outsize):
        if isinstance(insize, int): insize = [insize]
        dims = insize + [outsize]
        self.reset_gate = Layer(dims, outsize, f=sigmoid)
        self.update_gate = Layer(dims, outsize, f=sigmoid)
        self.input_layer = Layer(dims, outsize)
        self.h0 = zeros((outsize,)) # or parameter?

    def start(self, state=None):
        if state is None:
            state = self.h0
        self.h = state

    def state(self):
        return self.h

    def step(self, *inp, **kwargs):
        mask = _get_mask(kwargs)
        r = self.reset_gate(*(inp + (self.h,)))
        z = self.update_gate(*(inp + (self.h,)))
        h_tilde = self.input_layer(*(inp + (r*self.h,)))
        h = (constant(1.)-z)*self.h + z*h_tilde
        self.h = where(mask, h, self.h)
        return self.h
GRU = GatedRecurrentUnit

class LongShortTermMemory(Transducer):
    """Long short-term memory recurrent network.

    This version is from: Alex Graves, "Generating sequences with
    recurrent neural networks," arXiv:1308.0850, which has:

    - diagonal peephole connections
    - output activation function
    - differently from Graves 2013, there is no forget gate; in its
      place is one minus the input gate.

    :param insize:  number of input units.
    :param outsize: number of output units.
    :param f:       activation function (default tanh)
    """

    def __init__(self, insize, outsize):
        if isinstance(insize, int): insize = [insize]
        dims = insize + [outsize, "diag"]
        self.input_gate = Layer(dims, outsize, f=sigmoid)
        #self.forget_gate = Layer(dims, outsize, f=sigmoid, bias=5.)
        self.output_gate = Layer(dims, outsize, f=sigmoid)
        self.input_layer = Layer(dims[:-1], outsize, f=tanh)
        self.h0 = zeros((outsize,)) # or parameter?
        self.c0 = zeros((outsize,)) # or parameter?

    def state(self):
        return (self.h, self.c)

    def start(self, state=None):
        if state is None:
            h, c = self.h0, self.c0
        else:
            h, c = state
        self.h, self.c = h, c

    def step(self, *inp, **kwargs):
        mask = _get_mask(kwargs)
        i = self.input_gate(*(inp + (self.h, self.c)))
        #f = self.forget_gate(*(inp + (self.h, self.c)))
        f = constant(1.) - i
        c = f * self.c + i * self.input_layer(*(inp + (self.h,)))
        self.c = where(mask, c, self.c)
        o = self.output_gate(*(inp + (self.h, self.c)))
        h = o * tanh(self.c)
        self.h = where(mask, h, self.h)
        return self.h
LSTM = LongShortTermMemory
