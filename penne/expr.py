"""Building and using expressions."""

from . import backend
from . import calltree
import six
from six.moves import range
import operator
import numpy

__all__ = ["Expression", "Unary", "Binary", "Reduction",
           "constant", "one_hot", "zeros", "ones", "full", "arange",
           "parameter",
           "take",
           "add", "lsum", "subtract", "multiply", "divide", "negative", "power", "square",
           "maximum", "minimum", "clip", "where",
           "exp", "log", "tanh",
           "asum", "amax", "amin", "mean",
           "dot", "matmul", "tensordot", "inner",
           "vecdot",
           "concatenate", "stack", 
           "transpose", "swapaxes", "moveaxis",
           "reshape", "ravel", "expand_dims", "squeeze",
           "setitem",
           "topological"]

class Expression(object):
    """Base class for expression classes."""
    serial = 0
    def __init__(self, *args):
        for i, arg in enumerate(args):
            if not isinstance(arg, Expression):
                raise TypeError("expected Expression as argument %d, got %s" % (i, type(arg)))
        self.args = args
        self.has_parameter = isinstance(self, parameter) or any(arg.has_parameter for arg in args)

        self.forward()

        self.serial = Expression.serial
        Expression.serial += 1

        if calltree.calltree_enabled:
            frame = calltree.calltree_stack[-1]
            while frame.contents != self.__init__.__code__:
                frame = frame.parent
            # replace code object with class
            self.frame = calltree.Frame(self.__class__, frame.plineno, frame.plasti, frame.parent)

    shape = property(lambda self: self.value.shape)
    ndim = property(lambda self: self.value.ndim)
    dtype = property(lambda self: self.value.dtype)
    def __len__(self): return len(self.value)

    def __str__(self):
        args = ["<%s>" % arg.serial for arg in self.args]
        return "%s(%s)" % (self.__class__.__name__, ', '.join(args))

    def forward(self):
        """Compute self.value given values of self.args."""
        raise NotImplementedError()

    def backward(self, gradients):
        """Update gradient with respect to args given gradient with respect to self.

        pre/post: gradients[self] has the same shape as self.value.
        pre: gradients[self.args[i]] is either 0.0 or has the same shape as self.args[i].value.
        post: gradients[self.args[i]] has the same shape as self.args[i].value.
        """
        pass

    def __getitem__(self, item):   return getitem(self, item)
    def __setitem__(self, item, x):
        raise NotImplementedError("use a = setitem(a, item, b) instead")

    def __add__(self, other):      return add(self, other)
    def __sub__(self, other):      return subtract(self, other)
    def __mul__(self, other):      return multiply(self, other)
    def __div__(self, other):      return divide(self, other)
    def __truediv__(self, other):  return true_divide(self, other)
    def __pow__(self, other):      return power(self, other)
    def __pos__(self):             return self
    def __neg__(self):             return negative(self)

    def __lt__(self, other):       return less(self, other)
    def __le__(self, other):       return less_equal(self, other)
    def __gt__(self, other):       return greater(self, other)
    def __ge__(self, other):       return greater_equal(self, other)

    def dot(self, other):          return dot(self, other)
    def __matmul__(self, other):   return matmul(self, other)

    def reshape(self, *args):
        if len(args) == 1 and type(args) is tuple:
            return reshape(self, args[0])
        else:
            return reshape(self, args)

    def transpose(self, axes=None): return transpose(self, axes)
    T = property(transpose)

    def sum(self, axis=None, keepdims=False):    return asum(self, axis, keepdims)
    def min(self, axis=None, keepdims=False):    return amin(self, axis, keepdims)
    def max(self, axis=None, keepdims=False):    return amax(self, axis, keepdims)
    def argmin(self, axis=None, keepdims=False): return argmin(self, axis, keepdims)
    def argmax(self, axis=None, keepdims=False): return argmax(self, axis, keepdims)
    def mean(self, axis=None, keepdims=False):   return amean(self, axis, keepdims)

class constant(Expression):
    """A constant value.

    :param value: The value of the new expression.
    :type value: NumPy array
    """
    def __init__(self, value):
        self.value = backend.asarray(value)
        Expression.__init__(self)

    def forward(self):
        pass

    def __str__(self):
        return "constant(%s)" % (self.value,)

def one_hot(n, idx, axis=-1):
    return constant(backend.one_hot(n, idx, axis))

def zeros(shape):
    return constant(backend.zeros(shape))
def ones(shape):
    return constant(backend.ones(shape))
def full(shape, value):
    return constant(backend.full(shape, value))

def arange(stop):
    return constant(backend.arange(stop))

class parameter(Expression):
    """A parameter that is to be trained.

    :param value: The initial value of the new parameter.
    :type value: Numpy array
    """

    def __init__(self, value):
        self.value = backend.asarray(value)
        Expression.__init__(self)

    def forward(self):
        pass

    def __str__(self):
        return "parameter(%s)" % (self.value,)

def take(a, idx, axis=None):
    idx = backend.asarray(idx)
    post = []

    if axis is None:
        a = ravel(a)
        axis = 0

    if axis < 0:
        axis += a.ndim

    if idx.ndim != 1:
        shp1 = a.shape[:axis] + idx.shape + a.shape[axis+1:]
        idx = backend.ravel(idx)
        post.append(lambda out: reshape(out, shp1))

    if axis != 0:
        a = moveaxis(a, axis, 0)
        post.append(lambda out: moveaxis(out, 0, axis))

    if a.ndim != 2:
        shp2 = (-1,) + a.shape[1:]
        a = reshape(a, (a.shape[0], -1))
        post.append(lambda out: reshape(out, shp2))

    out = take1(a, idx)
    for f in reversed(post):
        out = f(out)
    return out

class take1(Expression):
    """Equivalent to take(a, idx, axis=0), where a is a 2-dimensional
    array, and idx is a 1-dimensional array. (Similar to libgpuarray's
    take1, but even more restricted.)"""

    def __init__(self, a, idx):
        idx = backend.asarray(idx)

        if idx.ndim != 1:
            raise ValueError('idx must have ndim 1')
        if a.ndim != 2:
            raise ValueError('a must have ndim 2')

        # handle negative indices
        idx = backend.where(idx >= 0, idx, idx+a.shape[0])

        self.idx = backend.csr_matrix((backend.ones(idx.shape, dtype=a.dtype),
                                       idx,
                                       backend.arange(idx.shape[0]+1)),
                                      shape=(idx.shape[0], a.shape[0]))
        Expression.__init__(self, a)

    def forward(self):
        self.value = backend.sparse_dense(self.idx, self.args[0].value)

    def backward(self, gradients):
        a, = self.args
        gradients[a] = backend.add_sparse_dense(self.idx.T, gradients[self], gradients[a])

class Unary(Expression):
    """Base class for unary componentwise operations."""
    def __init__(self, x):
        Expression.__init__(self, x)

    def forward(self):
        x, = self.args
        self.value = self.f(x.value)

    def backward(self, gradients):
        x, = self.args
        dfdx = self.dfdx(x.value, self.value)
        if x.has_parameter:
            gradients[x] += contract_like(dfdx * gradients[self], x.value)

class Binary(Expression):
    """Base class for binary componentwise operations."""
    def __init__(self, x, y):
        Expression.__init__(self, x, y)

    def forward(self):
        x, y = self.args
        self.value = self.f(x.value, y.value)

    def backward(self, gradients):
        x, y = self.args
        dfdx = self.dfdx(x.value, y.value, self.value)
        dfdy = self.dfdy(x.value, y.value, self.value)
        if x.has_parameter:
            gradients[x] += contract_like(dfdx * gradients[self], x.value)
        if y.has_parameter:
            gradients[y] += contract_like(dfdy * gradients[self], y.value)

## Arithmetic operations

class add(Binary):
    f = staticmethod(lambda x,y: backend.add(x,y))

    # "Inline" df to avoid multiplications by 1 in this very common case
    def backward(self, gradients):
        x, y = self.args
        if x.has_parameter:
            gradients[x] += contract_like(gradients[self], x.value)
        if y.has_parameter:
            gradients[y] += contract_like(gradients[self], y.value)

class lsum(Expression):
    """Adds the arrays in a list of arrays."""
    def __init__(self, args):
        Expression.__init__(self, *args)

    def forward(self):
        self.value = 0.
        for arg in self.args:
            try:
                self.value += arg.value
            except ValueError:
                self.value = self.value + arg.value

    def backward(self, gradients):
        for arg in self.args:
            if arg.has_parameter:
                gradients[arg] += contract_like(gradients[self], arg.value)

class subtract(Binary):
    f = staticmethod(lambda x,y: backend.subtract(x,y))
    dfdx = staticmethod(lambda x,y,z: 1.)
    dfdy = staticmethod(lambda x,y,z: -1.)

class negative(Unary):
    f = staticmethod(lambda x: backend.negative(x))
    dfdx = staticmethod(lambda x,z: -1.)

class multiply(Binary):
    f = staticmethod(lambda x,y: backend.multiply(x,y))
    dfdx = staticmethod(lambda x,y,z: y)
    dfdy = staticmethod(lambda x,y,z: x)

def divide(x, y):
    if six.PY2 and backend.result_type(x.value, y.value).kind in ['i', 'u']:
            raise TypeError('Floor division not supported')
    else:
        return true_divide(x, y)

class true_divide(Binary):
    f = staticmethod(lambda x,y: backend.true_divide(x,y))
    dfdx = staticmethod(lambda x,y,z: 1./y)
    dfdy = staticmethod(lambda x,y,z: -x/y**2)

class power(Binary):
    f = staticmethod(lambda x,y: backend.power(x,y))
    dfdx = staticmethod(lambda x,y,z: z*y/x)
    dfdy = staticmethod(lambda x,y,z: z*backend.log(x))

def square(x):
    return x*x

class log(Unary):
    f = staticmethod(lambda x: backend.log(x))
    dfdx = staticmethod(lambda x,z: 1./x)

class exp(Unary):
    f = staticmethod(lambda x: backend.exp(x))
    dfdx = staticmethod(lambda x,z: z)

class tanh(Unary):
    f = staticmethod(lambda x: backend.tanh(x))
    @staticmethod
    def dfdx(x, z):
        try:
            return backend.onemxx(z)
        except AttributeError:
            return 1.-z**2
    gain = 1.

class maximum(Binary):
    f = staticmethod(lambda x,y: backend.maximum(x,y))
    dfdx = staticmethod(lambda x,y,z: (x > y).astype(float))
    dfdy = staticmethod(lambda x,y,z: (x <= y).astype(float))

class minimum(Binary):
    f = staticmethod(lambda x,y: backend.minimum(x,y))
    dfdx = staticmethod(lambda x,y,z: (x < y).astype(float))
    dfdy = staticmethod(lambda x,y,z: (x >= y).astype(float))

def clip(x, lo, hi):
    return minimum(maximum(x, lo), hi)

## Conditionals

class where(Expression):
    def __init__(self, c, x, y):
        self.c = backend.asarray(c)
        Expression.__init__(self, x, y)

    def forward(self):
        x, y = self.args
        self.value = backend.where(self.c, x.value, y.value)

    def backward(self, gradients):
        x, y = self.args
        if x.has_parameter:
            gradients[x] += contract_like(self.c.astype(bool) * gradients[self], x.value)
        if y.has_parameter:
            gradients[y] += contract_like(backend.logical_not(self.c) * gradients[self], x.value)

### Reductions

def _keepdims(shp, axis, keepdims):
    if keepdims:
        return shp
    if axis is None:
        # should be (1,)*ndim but this works too
        return ()
    if type(axis) is int:
        if axis < 0: axis += len(shp)+1
        return shp[:axis] + (1,) + shp[axis:]
    if type(axis) is tuple:
        shp = list(shp)
        nd = len(shp)+len(axis)
        axis = [i if i >= 0 else i+nd for i in axis]
        for i in axis:
            shp[i:i] = [1]
        return tuple(shp)
    raise ValueError("invalid axis")

class Reduction(Expression):
    """Base class for reduction operations."""

    def __init__(self, x, axis=None, keepdims=False):
        self.axis = axis
        self.keepdims = keepdims
        Expression.__init__(self, x)

    def forward(self):
        x, = self.args
        self.value = self.f(x.value, axis=self.axis, keepdims=self.keepdims)

    def backward(self, gradients):
        x, = self.args
        if not x.has_parameter:
            return

        shp = _keepdims(self.value.shape, self.axis, self.keepdims)
        gself = backend.asarray(gradients[self]).reshape(shp)
        vself = backend.asarray(self.value).reshape(shp)

        df = self.df(x.value, vself, axis=self.axis)
        gradients[x] = ensure_shape(gradients[x], x.shape)
        gradients[x] += gself * df

class sum(Reduction):
    f = staticmethod(lambda x,axis,keepdims: backend.sum(x,axis=axis,keepdims=keepdims))
    df = staticmethod(lambda x,y,axis: 1.)
asum = sum

class amax(Reduction):
    f = staticmethod(lambda x,axis,keepdims: backend.amax(x,axis=axis,keepdims=keepdims))
    @staticmethod
    def df(x, y, axis):
        is_max = (x == y).astype(x.dtype)
        z = backend.asum(is_max, axis=axis, keepdims=True)
        return is_max / z
max = amax

class amin(Reduction):
    f = staticmethod(lambda x,axis,keepdims: backend.amin(x,axis=axis,keepdims=keepdims))
    @staticmethod
    def df(x, y, axis):
        is_min = (x == y).astype(x.dtype)
        return is_min / backend.asum(is_min, axis=axis, keepdims=True)
min = amin

class mean(Reduction):
    f = staticmethod(lambda x,axis,keepdims: backend.mean(x,axis=axis,keepdims=keepdims))
    df = staticmethod(lambda x,y,axis: float(y.size) / x.size)

class vecdot(Expression):
    """Equivalent to sum(x*y, axis).

    :type x: Expression
    :type y: Expression
    :param axis: axis or axes along which to perform the sum-product.
    :type axis: int or tuple of ints
    :param keepdims: leave axis or axes in result as dimensions with size one.
    :type keepdims: bool
    """
    def __init__(self, x, y, axis=None, keepdims=False):
        self.axis = axis
        self.keepdims = keepdims
        if x.ndim != y.ndim:
            raise ValueError("arguments must have the same number of axes")
        if axis is None:
            axis = range(x.ndim)
        elif isinstance(axis, int):
            axis = (axis,)
        for a in axis:
            if x.shape[a] != y.shape[a]:
                raise ValueError("arguments must have same size along reduction axis")
        Expression.__init__(self, x, y)
    
    def forward(self):
        x, y = self.args
        self.value = backend.vecdot(x.value, y.value, self.axis, self.keepdims)

    def backward(self, gradients):
        x, y = self.args
        shp = _keepdims(self.shape, self.axis, self.keepdims)
        gself = backend.asarray(gradients[self]).reshape(shp)
        if x.has_parameter:
            gradients[x] += contract_like(gself * y.value, x.value)
        if y.has_parameter:
            gradients[y] += contract_like(gself * x.value, y.value)

### Product-like operations

class dot2(Expression):
    def __init__(self, x, y):
        Expression.__init__(self, x, y)

    def forward(self):
        x, y = self.args
        self.value = backend.dot(x.value, y.value)

    def backward(self, gradients):
        x, y = self.args
        xd, yd = x.ndim, y.ndim
        gz = gradients[self]
        if x.has_parameter:
            yv = y.value
            if y.ndim > 1:
                gradients[x] = backend.add_dot(gz, yv.T, gradients[x])
            elif x.ndim > 1:
                gradients[x] = backend.add_outer(gz, yv, gradients[x])
            else:
                gradients[x] += backend.dot(gz, yv)
        if y.has_parameter: 
            xv = x.value
            if x.ndim > 1:
                gradients[y] = backend.add_dot(xv.T, gz, gradients[y])
            elif y.ndim > 1:
                gradients[y] = backend.add_outer(xv, gz, gradients[y])
            else:
                gradients[y] += backend.dot(xv, gz)

def dot(x, y):
    xd, yd = x.ndim, y.ndim
    if yd > 2: y = moveaxis(y, -2, 0)
    zshape = x.shape[:-1] + y.shape[1:]
    if xd > 2: x = reshape(x, (-1, x.shape[-1]))
    if yd > 2: y = reshape(y, (y.shape[0], -1))
    z = dot2(x, y)
    if z.shape != zshape: z = reshape(z, zshape)
    return z

def matmul(x, y):
    if x.shape == 0 or y.shape == 0:
        raise ValueError("Scalar operands are not allowed, use '*' instead")
    if x.shape > 2 and y.shape > 2:
        raise NotImplementedError("Stacks of matrices are not implemented")
    return dot(x, y)

def prod(xs):
    return reduce(operator.mul, xs, 1)

def tensordot(a, b, axes=2):
    """Like numpy.tensordot, but axes must be an int."""
    sa = a.shape[:-axes]
    sb = b.shape[axes:]
    if a.ndim > 2: a = reshape(a, (prod(sa), -1))
    if b.ndim > 2: b = reshape(b, (-1, prod(sb)))
    c = dot2(a, b)
    if c.shape != sa+sb: c = reshape(c, sa+sb)
    return c

def inner(a, b):
    cshape = a.shape[:-1] + b.shape[:-1]
    if b.ndim > 1: b = moveaxis(b, -1, 0)
    if a.ndim > 2: a = reshape(a, (-1, a.shape[-1]))
    if b.ndim > 2: b = reshape(b, (b.shape[0], -1))
    c = dot2(a, b)
    if c.shape != cshape: c = reshape(c, cshape)
    return c

### Cutting and pasting

class concatenate(Expression):
    def __init__(self, args, axis=0):
        if not isinstance(axis, int):
            raise TypeError("axis must be an int")
        self.axis = axis
        Expression.__init__(self, *args)

    def forward(self):
        self.value = backend.concatenate([arg.value for arg in self.args], axis=self.axis)

    def backward(self, gradients):
        i = 0
        s = [slice(None)]*backend.ndim(gradients[self])
        for arg in self.args:
            d = arg.shape[self.axis]
            if arg.has_parameter:
                s[self.axis] = slice(i, i+d)
                gradients[arg] += gradients[self][tuple(s)]
            i += d

class stack(Expression):
    def __init__(self, args, axis=0):
        if not isinstance(axis, int):
            raise TypeError("axis must be an int")
        self.axis = axis
        Expression.__init__(self, *args)

    def forward(self):
        self.value = backend.stack([arg.value for arg in self.args], axis=self.axis)

    def backward(self, gradients):
        s = [slice(None)]*backend.ndim(gradients[self])
        for i, arg in enumerate(self.args):
            if arg.has_parameter:
                s[self.axis] = i
                gradients[arg] += gradients[self][tuple(s)]

class reshape(Expression):
    def __init__(self, arg, shape):
        self.new_shape = shape
        Expression.__init__(self, arg)

    def forward(self):
        self.value = backend.reshape(self.args[0].value, self.new_shape)

    def backward(self, gradients):
        arg = self.args[0]
        if arg.has_parameter:
            gradients[arg] += backend.reshape(gradients[self], arg.shape)

def ravel(a):
    return reshape(a, (-1,))

def expand_dims(a, axis):
    if axis < 0: axis += a.ndim+1
    shape = a.shape[:axis] + (1,) + a.shape[axis:]
    return reshape(a, shape)

def squeeze(a, axis):
    """Like numpy.squeeze, but axis can't be None."""
    if a.shape[axis] != 1:
        raise ValueError('can only squeeze an axis of size one')
    shape = a.shape[:axis] + a.shape[axis:]
    return reshape(a, shape)

class transpose(Expression):
    def __init__(self, arg, axes=None):
        self.axes = axes
        Expression.__init__(self, arg)

    def forward(self):
        self.value = backend.transpose(self.args[0].value, self.axes)

    def backward(self, gradients):
        arg = self.args[0]
        raxes = None if self.axes is None else list(numpy.argsort(self.axes))
        if arg.has_parameter:
            gradients[arg] += backend.transpose(gradients[self], raxes)

def swapaxes(a, ax1, ax2):
    axes = list(range(a.ndim))
    axes[ax1], axes[ax2] = axes[ax2], axes[ax1]
    return transpose(a, axes)

def moveaxis(a, src, dst):
    """Like numpy.moveaxis, but only allows single axis."""
    axes = list(range(a.ndim))
    axes.insert(dst, axes.pop(src))
    return transpose(a, axes)

class Index(object):
    def __init__(self, idx):
        if isinstance(idx, Expression):
            raise IndexError("index cannot be Expression")
        if not isinstance(idx, tuple):
            idx = (idx,)
        self.idx = idx

    # Workaround because Ellipsis is not picklable
    def __getstate__(self):
        d = dict(self.__dict__)
        d['idx'] = tuple("..." if s is Ellipsis else s for s in d['idx'])
        return d
    def __setstate__(self, d):
        self.__dict__ = d
        self.idx = tuple(Ellipsis if s == "..." else s for s in self.idx)

    def __str__(self):
        result = []
        for i in self.idx:
            if isinstance(i, slice): 
                start = i.start if i.start is not None else ""
                stop = i.stop if i.stop is not None else ""
                step = ":"+str(i.step) if i.step is not None else ""
                i = "{}:{}{}".format(start, stop, step)
            elif i is Ellipsis:
                i = "..."
            result.append(str(i))
        return ','.join(result)

class getitem(Expression):
    """
    :param arg: array to be indexed
    :param item: index
    :type item: NumPy-style index (not Expression)
    """
    def __init__(self, arg, item):
        self.item = Index(item)
        Expression.__init__(self, arg)

    def forward(self):
        self.value = self.args[0].value[self.item.idx]

    def backward(self, gradients):
        arg, = self.args
        if arg.has_parameter:
            gradients[arg] = ensure_shape(gradients[arg], arg.shape)
            gradients[arg][self.item.idx] += gradients[self]

    def __str__(self):
        return "%s[%s]" % (self.args[0], self.item)

class setitem(Expression):
    """A new expression with an index or slice modified.

    Note that this makes a copy of the whole array.

    :param x: array to be indexed
    :param item: index
    :type item: NumPy-style index (not Expression)
    :param y: new value
    """
    def __init__(self, x, item, y):
        self.item = Index(item)
        Expression.__init__(self, x, y)

    def forward(self):
        x, y = self.args
        self.value = x.value[...]
        self.value[self.item.idx] = y.value

    def backward(self, gradients):
        x, y = self.args
        if x.has_parameter:
            gradients[x] += gradients[self]
            gradients[x][self.item.idx] -= gradients[self][self.item.idx]
        if y.has_parameter:
            gradients[y] += gradients[self][self.item.idx]

    def __str__(self):
        return "%s[%s] = %s" % (self.args[0], self.item, self.args[1])

###

def topological(root):
    """Traverse an Expression in topological (bottom-up) order."""
    stack = [(root, 0)]
    result = []
    visited = set()
    while len(stack) > 0:
        x, i = stack.pop()
        if i == len(x.args):
            result.append(x)
        else:
            stack.append((x, i+1))
            if x.args[i] not in visited:
                stack.append((x.args[i], 0))
                visited.add(x.args[i])
    return result

### Utilities

def contract_like(a, b):
    """Sum axes of a so that shape is the same as b."""
    a = backend.asarray(a)
    b = backend.asarray(b)
    if a.shape == b.shape:
        return a
    else:
        b_shape = (1,)*(a.ndim-b.ndim) + b.shape
        axes = tuple(axis for axis in range(a.ndim) if a.shape[axis] > b_shape[axis] and b_shape[axis] == 1)
        if axes != ():
            a = backend.sum(a, axes, keepdims=True)
        return a.reshape(b.shape)

def ensure_shape(ary, shp):
    if backend.ndim(ary) == 0:
        return backend.full(shp, ary)
    elif ary.shape == shp:
        return ary
    else:
        raise NotImplementedError()

