"""Compute values, gradients, and other things."""

__all__ = ['compute_value', 'compute_gradients', 'check_gradients']

from . import expr # parameter, topological
from . import debug

from . import backend
import numpy
import time
import sys

### Forward and backward propagation

def compute_value(x):
    """Recompute the value of an expression.
    :param x:      expression to compute value of.
    :return:       value of x (= x.value)
    """

    for subx in expr.topological(x):
        subx.forward()
    return x.value

def compute_gradients(x):
    """Compute gradients using automatic differentiation.

    :param x:      expression to compute gradients of.
    :return:       dictionary from subexpressions to computed gradients.
    """

    if not x.has_parameter:
        return {}
    if x.ndim != 0:
        raise ValueError('can only compute gradients of scalars')

    gradients = {x: 1.}
    for subx in reversed(expr.topological(x)):
        if not subx.has_parameter:
            continue
        for arg in subx.args:
            if arg.has_parameter:
                gradients.setdefault(arg, 0.)
            
        try:
            if debug.profile: start_time = time.clock()
            subx.backward(gradients)
            if debug.profile: 
                for arg in subx.args:
                    if arg in gradients and hasattr(gradients[arg], 'sync'):
                        gradients[arg].sync()
                stop_time = time.clock()

        except:
            if hasattr(subx, 'frame'): debug.print_traceback(subx)
            raise

        if debug.profile:
            tim = stop_time-start_time
            gx = backend.asarray(gradients[subx])
            mem = gx.size * gx.itemsize
            debug.profile_increment(subx, tim, mem)

        if not isinstance(subx, expr.parameter):
            del gradients[subx]

    return gradients

def check_gradients(x, delta=1e-6, threshold=0.01):
    """Compute gradients using symmetric differences and check against
    gradients computed by ``compute_gradients``.

    Prints out discrepant gradients and raises a ValueError if the
    check fails.  This is extremely slow and is used only for
    debugging.

    :param x:         expression to compute gradients of.
    :param delta:     size of change in parameter value.
    :type delta:      float
    :param threshold: maximum tolerated error.
    :type threshold:  float
    """

    params = [subx for subx in reversed(expr.topological(x)) if isinstance(subx, expr.parameter)]
    gradients = compute_gradients(x)

    for param in params:
        g = numpy.zeros(param.shape)
        for idx in numpy.ndindex(param.shape):
            save = param.value[idx]
            param.value[idx] -= delta/2
            val_minus = compute_value(x)
            param.value[idx] += delta
            val_plus = compute_value(x)
            param.value[idx] = save
            g[idx] = (val_plus-val_minus)/delta
        if not numpy.allclose(g, gradients[param], rtol=threshold):
            sys.stderr.write("gradient (finite difference): %s\n" % g)
            sys.stderr.write("gradient (automatic): %s\n" % gradients[param])
            sys.stderr.write("error: %s\n" % ((g-gradients[param])/gradients[param]))
            if hasattr(param, 'frame'): debug.print_traceback(param)
            raise ValueError("gradients do not match")
