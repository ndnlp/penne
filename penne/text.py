"""Utilities for working with natural language text."""

from __future__ import division
import collections
import six
from six.moves import range, zip, zip_longest
import numpy
import random

def shuffled(data):
    data = list(data)
    random.shuffle(data)
    return data

def minibatches(data, n):
    """Iterator over minibatches of training examples.

    All batches but the last have n elements; the last batch
    may have fewer elements.

    :param data: input data
    :type data:  iterable
    :param n:    minibatch size
    :type n:     int
    :rtype:      iterator over lists"""

    m = []
    for x in data:
        m.append(x)
        if len(m) == n:
            yield m
            m = []
    if m:
        yield m

def pack_minibatch(mb, fillvalue=-1):
    """Converts a minibatch from "minibatch-first" order to
    "minibatch-second" order. Right-pads sentences with fillvalue.

    In "minibatch-first" order, batch[i][j] is sentence i, word j.
    In "minibatch-second" order, batch[i][j] is sentence j, word i.

    :param batch: minibatch in "minibatch-first" order
    :type batch:  list of lists
    :return:      minibatch in "minibatch-second" order
    :rtype:       list of lists
    """

    return numpy.array(list(zip_longest(*mb, fillvalue=fillvalue)))

class Numberizer(object):
    """Class for converting between words and numbers.

    :param words: vocabulary (as returned by make_vocab).
    :type words:  iterable
    :param unk:   value used for out-of-vocabulary words."""

    def __init__(self, words, unk="<unk>"):
        self.w = list(sorted(set(words) | {unk}))
        self.n = {word:number for number, word in enumerate(self.w)}
        self.unk = self.n[unk]

    def num(self, word):
        """Convert a word into a number."""
        return self.n.get(word, self.unk)

    def nums(self, words):
        """Convert a list of words into a list of numbers."""
        return numpy.array(map(self.num, words))

    def word(self, num):
        """Convert a number into a word."""
        return self.w[num]

    def words(self, nums):
        """Convert a list of numbers into a list of words."""
        return list(map(self.word, nums))
    
    def __len__(self):
        """The number of word types in the vocabulary."""
        return len(self.w)

def make_vocab(data, size=None, special=['<s>', '</s>', '<unk>']):
    """Make a vocabulary.

    :param data:    data (as returned by read_data)
    :type data:     list of lists of strs
    :param size:    limit vocabulary to this many types (optional)
    :type size:     int
    :param special: include these words in the vocabulary even if not in data
    :type special:  iterable of strs
    """
    c = collections.Counter()
    for words in data:
        for word in words:
            c[word] += 1

    for word in special:
        if word in c:
            del c[word]

    if size:
        vocab = {word for word, count in c.most_common(size-len(special))}
    else:
        vocab = set(c)
    vocab.update(special)

    return vocab

class TextReader(object):
    def __init__(self, nr):
        self.nr = nr
        self.total_lines = 0
        self.total_words = 0

    @staticmethod
    def from_file(filename, vocab_size=None):
        nr = Numberizer(make_vocab([line.split() for line in open(filename)], size=vocab_size))
        return TextReader(nr)

    def read_lines(self, filename, max_words=None):
        for line in open(filename):
            words = line.split()
            if max_words and len(words) > max_words:
                continue
            yield self.nr.nums(['<s>'] + words + ['</s>'])
            self.total_lines += 1
            self.total_words += len(words)

    def line_to_str(self, nums):
        words = self.nr.words(nums)
        while len(words) > 0 and words[-1] == '</s>':
            words.pop()
        return ' '.join(words)

    def read_minibatches(self, filename, minibatch_size, shuffle=False, midibatch_size=None, max_words=None):
        lines = self.read_lines(filename, max_words=max_words)
        if shuffle and midibatch_size:
            midibatches = minibatches(lines, midibatch_size)
        else:
            midibatches = [lines]

        for midibatch in midibatches:
            if shuffle:
                midibatch = sorted(midibatch, key=len)
            mbs = minibatches(midibatch, minibatch_size)
            if shuffle:
                mbs = shuffled(mbs)
            for mb in mbs:
                yield pack_minibatch(mb)

    def minibatch_lengths(self, mb):
        """Return an array of the lengths of the sentences in a packed minibatch."""
        return numpy.sum(mb >= 0, axis=0)

    def read_ngrams(self, filename, order):
        for line in open(filename):
            words = ['<s>']*(order-1) + line.split() + ['</s>']
            nums = self.nr.nums(words)
            for i in range(len(nums)-order+1):
                yield nums[i:i+order]

    def read_ngram_minibatches(self, filename, order, minibatch_size, shuffle=False, midibatch_size=None):
        ngrams = self.read_ngrams(filename, order)
        if shuffle and midibatch_size:
            midibatches = minibatches(ngrams, midibatch_size)
        else:
            midibatches = [ngrams]

        for midibatch in midibatches:
            mbs = minibatches(midibatch, minibatch_size)
            if shuffle:
                mbs = shuffled(mbs)
            for mb in mbs:
                yield pack_minibatch(mb)

class MultitextReader(object):
    def __init__(self, readers):
        self.readers = readers

    def __getitem__(self, i):
        return self.readers[i]

    @staticmethod
    def from_files(filenames, vocab_size=None):
        readers = [TextReader.from_file(filename, vocab_size) for filename in filenames]
        return MultitextReader(readers)

    def read_lines(self, filenames, max_words=None):
        for lines in zip(*[reader.read_lines(filename) for reader, filename in zip(self.readers, filenames)]):
            if max_words and any(len(line)-1 > max_words for line in lines):
                continue
            yield lines

    def read_minibatches(self, filenames, minibatch_size, shuffle=False, midibatch_size=None, max_words=None):
        lines = self.read_lines(filenames, max_words=max_words)
        if shuffle and midibatch_size:
            midibatches = minibatches(lines, midibatch_size)
        else:
            midibatches = [lines]
            
        for midibatch in midibatches:
            if shuffle:
                midibatch = sorted(midibatch, key=lambda sents: sum(map(len, sents)))
            mbs = minibatches(midibatch, minibatch_size)
            if shuffle:
                mbs = shuffled(mbs)
            for mb in mbs:
                yield tuple(map(pack_minibatch, zip(*mb)))
