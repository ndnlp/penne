"""Wrapper for some cuSPARSE routines."""
# with borrowing from
# https://github.com/lebedov/scikit-cuda/blob/master/skcuda/cusparse.py

__all__ = ['csr_matrix', 'csc_matrix', 'sparse_dense', 'add_sparse_dense']

import numpy
import pygpu
import ctypes

for lib in ['libcusparse.so', 'libcusparse.dylib']:
    try:
        cusparse = ctypes.CDLL(lib)
        break
    except:
        pass
else:
    raise OSError("couldn't load libcusparse")

handle = ctypes.c_void_p()
status = cusparse.cusparseCreate(ctypes.byref(handle))

errors = {
    0: 'the operation completed successfully',
    1: 'the library was not initialized',
    2: 'the resources could not be allocated',
    3: 'invalid parameters were passed',
    4: 'the device does not support double precision',
    6: 'the function failed to launch on the GPU',
    7: 'an internal operation failed',
    8: 'the matrix type is not supported',
}

class compressed_array(object):
    def __init__(self, args, indmax=None):
        data, indices, indptr = args
        if not data.flags.contiguous:
            data = data.copy()
        if not (indices.dtype == numpy.intc and indices.flags.contiguous):
            indices = indices.astype(numpy.intc)
        if not (indptr.dtype == numpy.intc and indptr.flags.contiguous):
            indptr = indptr.astype(numpy.intc)
        self.data, self.indices, self.indptr = data, indices, indptr

        if indmax is None:
            indmax = max(self.indices)+1
        self.indmax = indmax

    dtype = property(lambda self: self.data.dtype)

class csr_matrix(compressed_array):
    def __init__(self, args, shape=None):
        indmax = None if shape is None else shape[1]
        compressed_array.__init__(self, args, indmax)
        if shape is not None and shape != self.shape: 
            raise ValueError('inconsistent shape')

    def transpose(self):
        m, n = self.shape
        return csc_matrix((self.data, self.indices, self.indptr), (n, m))
    T = property(transpose)

    shape = property(lambda self: (len(self.indptr)-1, self.indmax))

class csc_matrix(compressed_array):
    def __init__(self, args, shape=None):
        indmax = None if shape is None else shape[0]
        compressed_array.__init__(self, args, indmax)
        if shape is not None and shape != self.shape: 
            raise ValueError('inconsistent shape')

    def transpose(self):
        m, n = self.shape
        return csr_matrix((self.data, self.indices, self.indptr), (n, m))
    T = property(transpose)

    shape = property(lambda self: (self.indmax, len(self.indptr)-1))

def sparse_dense(a, b):
    return csrmm(a, b)

def add_sparse_dense(a, b, c):
    if not isinstance(c, pygpu.gpuarray.GpuArray) or c.ndim == 0 or c.strides[0] != c.itemsize:
        return c + csrmm(a, b)
    else:
        return csrmm(a, b, beta=1., c=c)

def csrmm(a, b, alpha=1., beta=0., c=None):
    # Validate a

    if isinstance(a, csr_matrix):
        transA = False
    elif isinstance(a, csc_matrix):
        transA = True
    else:
        raise TypeError('a must be csr_matrix or csc_matrix')
    nnz = a.indices.size

    # Validate b

    if b.ndim != 2:
        raise ValueError('b must have ndim 2')
    if b.shape[0] != a.shape[1]:
        raise ValueError('a and b have inconsistent shapes')

    if b.strides[0] == b.itemsize:
        transB = False
        ldb = b.strides[1]//b.itemsize
    elif b.strides[1] == b.itemsize:
        transB = True
        ldb = b.strides[0]//b.itemsize
    else:
        # CUSPARSE docs say that transposing B is faster
        b = b.copy(order='C')
        transB = True
        ldb = b.strides[0]//b.itemsize

    # csr<T>mm2 does not allow both A and B to be transposed.
    # We can either copy-transpose A or B -- not sure which is faster
    if transA and transB:
        a = csr2csc(a)
        transA = False

        """b = pygpu.gpuarray.array(b, order='F')
        transB = False"""

    # Validate c

    if c is None:
        c = pygpu.gpuarray.zeros((a.shape[0], b.shape[1]), dtype=a.dtype, order='F', cls=type(b))
    if c.shape != (a.shape[0], b.shape[1]):
        raise ValueError('c has wrong shape')
    if c.strides[0] != c.itemsize:
        raise ValueError('c must have contiguous columns')

    # Check types

    if not (a.dtype == b.dtype == c.dtype):
        raise TypeError('a, b, and c must have same dtype')
    if a.dtype == numpy.float32:
        csrTmm2 = cusparse.cusparseScsrmm2
        cfloat = ctypes.c_float
    elif a.dtype == numpy.float64:
        csrTmm2 = cusparse.cusparseDcsrmm2
        cfloat = ctypes.c_double
    else:
        raise TypeError('arrays must have dtype float32 or float64')

    if transA:
        k, m = a.shape
    else:
        m, k = a.shape
    n = b.shape[1]

    if transB:
        ldb = b.strides[0]//b.itemsize if b.shape[0] > 1 else b.shape[1]
    else:
        ldb = b.strides[1]//b.itemsize if b.shape[1] > 1 else b.shape[0]

    ldc = c.strides[1]//c.itemsize if c.shape[1] > 1 else c.shape[0]

    descrA = ctypes.c_void_p()
    status = cusparse.cusparseCreateMatDescr(ctypes.byref(descrA))
    try:
        status = csrTmm2(handle,
                         int(transA),
                         int(transB),
                         m, n, k, nnz,
                         ctypes.byref(cfloat(alpha)),
                         descrA,
                         ctypes.c_void_p(a.data.gpudata), 
                         ctypes.c_void_p(a.indptr.gpudata), 
                         ctypes.c_void_p(a.indices.gpudata),
                         ctypes.c_void_p(b.gpudata), ldb, 
                         ctypes.byref(cfloat(beta)),
                         ctypes.c_void_p(c.gpudata), ldc)
        if status:
            raise pygpu.gpuarray.GpuArrayException(errors[status])
    finally:
        status = cusparse.cusparseDestroyMatDescr(descrA)

    return c

def csr2csc(a, out=None):
    # Check shapes
    m, n = len(a.indptr)-1, a.indmax
    nnz = len(a.indices)

    if out is None:
        if isinstance(a, csr_matrix):
            cls = csc_matrix
        elif isinstance(a, csc_matrix):
            cls = csr_matrix
        else:
            raise TypeError('a must be a csr_matrix or csc_matrix')
        
        out = cls((pygpu.gpuarray.empty((nnz,), a.dtype),
                   pygpu.gpuarray.empty((nnz,), numpy.intc),
                   pygpu.gpuarray.empty((n+1,), numpy.intc)),
                  shape=a.shape)

    if (out.indmax, len(out.indptr)-1) != (m, n):
        raise ValueError('out has wrong shape')
    if len(out.indices) != nnz:
        raise ValueError('out has wrong nnz')

    # Check dtypes
    if not (a.indptr.dtype == a.indices.dtype == out.indptr.dtype == out.indices.dtype == numpy.intc):
        raise TypeError('a and out must have indices/pointers with dtype intc')

    if a.dtype != out.dtype:
        raise TypeError('a and out must have same dtype')
    if a.dtype == numpy.float32:
        cusparseTcsr2csc = cusparse.cusparseScsr2csc
    elif a.dtype == numpy.float64:
        cusparseTcsr2csc = cusparse.cusparseDcsr2csc
    else:
        raise TypeError('unsupported dtype')

    status = cusparseTcsr2csc(handle, 
                              m, n, nnz,
                              ctypes.c_void_p(a.data.gpudata), 
                              ctypes.c_void_p(a.indptr.gpudata), 
                              ctypes.c_void_p(a.indices.gpudata), 
                              ctypes.c_void_p(out.data.gpudata), 
                              ctypes.c_void_p(out.indices.gpudata), 
                              ctypes.c_void_p(out.indptr.gpudata), 
                              1, # copyValues=CUSPARSE_ACTION_NUMERIC
                              0) # idxBase=CUSPARSE_INDEX_BASE_ZERO
    if status:
        raise pygpu.gpuarray.GpuArrayException(errors[status])

    return out
