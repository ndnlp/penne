"""Special expressions for neural networks."""

__all__ = ['sigmoid', 'rectify', 'hardtanh', 'softmax', 'logsoftmax', 'crossentropy', 'distance2', 'Layer', 'EmbeddingLayer', 'Dropout', 'MaxoutLayer', 'Stack']

from six.moves import range
import numpy
from . import backend
from .expr import *

import logging
logger = logging.getLogger('penne')

## Activation functions

class sigmoid(Unary):
    r"""Logistic sigmoid function, :math:`\frac{1}{1+\exp -x}`."""
    gain = 0.25
    @staticmethod 
    def f(x):      
        #with backend.errstate(over='ignore'): 
        try:
            return backend.sigmoid(x)
        except AttributeError:
            return 1./(1.+backend.exp(-x))

    @staticmethod 
    def dfdx(x, y): 
        try:
            return backend.xonemx(y)
        except AttributeError:
            return y*(1.-y)

class rectify(Unary):
    r"""Rectified linear unit, :math:`\max(0, x)`."""
    gain = 0.5
    @staticmethod 
    def f(x):       return backend.maximum(x, 0.)
    @staticmethod 
    def dfdx(x, y): return backend.where(x > 0., 1., 0.)

class hardtanh(Unary):
    """Hard tanh function, equivalent to clip(x, -1, 1)."""
    gain = 1.
    @staticmethod 
    def f(x):       return backend.clip(x, -1., 1.)
    @staticmethod 
    def dfdx(x, y): return backend.where(backend.logical_and(-1. < x, x < 1.), 1., 0.)

class softmax(Expression):
    r"""Softmax function, :math:`y_i = \exp x_i / \sum_{i'} \exp x_{i'}`.
    
    :param axis: along which to perform the softmax (default is last).
    """
    gain = 1.
    def __init__(self, arg, axis=-1):
        self.axis = axis
        Expression.__init__(self, arg)

    def forward(self):
        axis = self.axis
        v = self.args[0].value
        v = v - backend.amax(v, axis=axis, keepdims=True)
        v = backend.exp(v)
        self.value = v / backend.sum(v, axis=axis, keepdims=True)

    def backward(self, gradients):
        axis = self.axis
        arg = self.args[0]
        if arg in gradients:
            gradients[arg] += (gradients[self] - backend.vecdot(self.value, gradients[self], axis=axis, keepdims=True)) * self.value

class logsoftmax(Expression):
    r"""Log-softmax function, :math:`y_i = \log \left(\exp x_i / \sum_{i'} \exp x_{i'}\right)`.
    
    Use this instead of log(softmax(x)) for better numerical stability.
    
    :param axis: along which to perform the softmax (default is last).
    """
    gain = 1.
    def __init__(self, arg, axis=-1):
        self.axis = axis
        Expression.__init__(self, arg)

    def forward(self):
        axis = self.axis
        v = self.args[0].value
        v = v - backend.amax(v, axis=axis, keepdims=True)
        self.value = v - backend.log(backend.sum(backend.exp(v), axis=axis, keepdims=True))

    def backward(self, gradients):
        axis = self.axis
        arg = self.args[0]
        if arg in gradients:
            gradients[arg] += gradients[self] - backend.sum(gradients[self], axis=axis, keepdims=True) * backend.exp(self.value)

### Dropout

class dropout(Expression):
    def __init__(self, factory, arg):
        self.factory = factory
        Expression.__init__(self, arg)

    def forward(self):
        if self.factory.enabled:
            p = self.factory.p
            arg = self.args[0]
            self.value = (backend.random.uniform(0., 1., arg.shape) > p) / (1-p)
        else:
            self.value = 1.

class Dropout(object):
    """Factory for dropouts.

    Example usage::

        d = Dropout(0.5)
        y = d(x)

    The reason for the extra level of indirection is so that all the
    dropouts can be enabled or disabled together.

    :param p: probability of dropout
    :type p:  float
    :return:  dropout function
    :rtype:   Expression -> Expression
    """
    def __init__(self, p=0.5):
        self.p = p
        self.enabled = False
    def __call__(self, arg):
        return arg * dropout(self, arg)
    def enable(self):
        """Enable all dropouts that originated from this factory."""
        self.enabled = True
    def disable(self):
        """Disable all dropouts that originated from this factory."""
        self.enabled = False

### Loss functions

def crossentropy(logp, correct):
    """Cross-entropy, a.k.a. log-loss.

    :param logp:    vector of log-probabilities
    :param correct: correct value
    """
    correct = numpy.asarray(correct, dtype=int)
    if correct.ndim == 0: 
        return -logp[correct]
    else:
        return -asum(logp*one_hot(logp.shape[-1], correct))

def distance2(x, y, axis=-1):
    """Squared Euclidean distance, a.k.a. mean-squared loss."""
    d = x - y
    return vecdot(d, d, axis=axis)

### Fully-connected layer

def guess_gain(f, d):
    """Try to figure out how the activation function affects the
    variance of inputs/gradients."""

    from .compute import compute_gradients

    if f is None: return 1.
    if hasattr(f, "gain"): return f.gain

    # As is standard, use the gradient of f at zero.  However, since f
    # might not be differentiable at zero (e.g., ReLU), compute
    # gradient a little bit to the left and right and average.

    delta = 0.1
    g = []
    for xv in [-delta, 0., delta]:
        x = parameter(xv)
        y = f(x)
        gradients = compute_gradients(y)
        g.append(gradients[x])

    if abs(g[2]-g[0])/2. > delta:
        return (g[0] + g[2]) / 2.
    else:
        return g[1]

def _random(variance, shape):
    #return backend.random.normal(0., variance**0.5, shape)
    r = (variance*3)**0.5
    logger.debug('initializing parameter with shape {} to uniform [{}, {}]'.format(shape, -r, r))
    return backend.random.uniform(-r, r, shape)

class Layer(object):
    """Factory for fully-connected layers.

    Example usage::

        l = Layer(100, 100)
        y = l(x)

    :param insize:  Input size or sequence of input sizes. 

                    - If an input size is n > 0, then that input will
                      expect an n-dimensional vector.

                    - If an input size is "diag", then that input will
                      have a diagonal weight matrix.

    :param outsize: Output size.
    :param f:       Activation function (default tanh).
    :param bias:    Initial bias, or None for no bias."""

    def __init__(self, insize, outsize, f=tanh, gain=None, bias=0.):
        if type(insize) is int: insize = [insize]

        if gain is None: gain = guess_gain(f, outsize)

        # Although it is more conventional to left-multiply by the weight
        # matrix, we right-multiply so it works correctly with stacks of
        # vectors.

        total_insize = 0
        for d in insize:
            if d == "diag":
                # Var[input * weight] = Var[input] * Var[weight]
                total_insize += 1
            elif d >= 0:
                # Var[dot(input, weight)] = d * Var[input] * Var[weight]
                total_insize += d
            else:
                raise ValueError('Invalid size (use EmbeddingLayer instead?)')
        if bias is not None: total_insize += 1
        variance = 2. / (total_insize + outsize) / gain**2

        self.weight = []
        for a, d in enumerate(insize):
            if d == "diag":
                w = parameter(_random(variance, (outsize,)))
                self.weight.append(w)

            else:
                w = parameter(_random(variance, (d, outsize)))
                self.weight.append(w)

        if bias is not None:
            self.bias = parameter(numpy.full((outsize,), bias))
        else:
            self.bias = None

        self.activation = f

    def __call__(self, *args):
        if len(args) != len(self.weight):
            raise TypeError("wrong number of inputs")
        s = []
        if self.bias: s.append(self.bias)
        for w, x in zip(self.weight, args):
            if w.value.ndim == 1: # diagonal
                s.append(x * w)
            else:
                s.append(dot(x, w)) # half-bug: what if x is a scalar?
        s = lsum(s)
        if self.activation:
            return self.activation(s)
        else:
            return s

class EmbeddingLayer(object):
    def __init__(self, insize, outsize):
        # Var[weight[input]] = Var[weight]
        variance = 2. / (1 + outsize)
        self.weights = parameter(_random(variance, (insize, outsize)))
    def __call__(self, x):
        return take(self.weights, x, axis=0)

class MaxoutLayer(object):
    """Maxout layer."""
    def __init__(self, insize, outsize, bias=0.):
        self.a = Layer(insize, outsize, f=None, bias=bias)
        self.b = Layer(insize, outsize, f=None, bias=bias)

    def __call__(self, *args):
        return maximum(self.a(*args), self.b(*args))

class Stack(object):
    def __init__(self, *layers):
        self.layers = layers

    def __call__(self, *inp):
        # ok for first layer to have multiple arguments
        v = self.layers[0](*inp)
        for layer in self.layers[1:]:
            v = layer(v)
        return v
