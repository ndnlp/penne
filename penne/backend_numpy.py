import numpy
from numpy import *
asum = numpy.sum

def one_hot(n, idx, axis=-1, dtype=float):
    """Creates a one-hot vector or a batch of one-hot vectors.

    If an index is out of bounds (including if it is negative), then
    the resulting vector is all zeros.

    :param n:     Size of the new axis to create.
    :param idx:   Index or indices to place ones.
    :type idx:    int or array[int]
    :param axis:  Which axis to create.
    :param dtype: Type of elements.

    """
    idx = asarray(idx)
    nd = idx.ndim
    if axis < 0: axis += nd+1
    idx = expand_dims(idx, axis)
    r = numpy.arange(n).reshape((n,)+(1,)*(nd-axis))
    return (idx == r).astype(dtype)

try:
    # N-dimensional
    from scipy.signal import convolve, correlate
except ImportError:
    # 1-dimensional
    def convolve(a, v, mode='full'):
        # bypass numpy.convolve because we don't want it to swap arguments
        return numpy.correlate(a, v[::-1].conj(), mode)
    correlate = numpy.correlate

def expand_like(a, b):
    return numpy.array(broadcast_to(a, numpy.shape(b)))

def add_outer(x, y, a):
    # would be faster by calling BLAS directly
    a += numpy.einsum('i,j->ij', x, y)
    return a

def add_dot(a, b, c):
    # would be faster by calling BLAS directly
    c += numpy.dot(a, b)
    return c

def vecdot(a, b, axis=None, keepdims=False, out=None):
    """Vector dot product along last axis; if a and b have other axes, they are
    aligned."""
    # would be faster with einsum
    return numpy.sum(a*b, axis=axis, keepdims=keepdims, out=out)

try:
    import scipy.sparse
    from scipy.sparse import csr_matrix, csc_matrix
    def sparse_dense(a, b):
        return a.dot(b)
    def add_sparse_dense(a, b, c):
        c += sparse_dense(a, b) # not very efficient
        return c
except ImportError:
    pass

# Functions new in NumPy 1.10

if 'stack' not in dir(numpy):
    def stack(arrays, axis=0):
        arrays = [numpy.asarray(arr) for arr in arrays]
        if not arrays:
            raise ValueError('need at least one array to stack')

        shapes = set(arr.shape for arr in arrays)
        if len(shapes) != 1:
            raise ValueError('all input arrays must have the same shape')

        result_ndim = arrays[0].ndim + 1
        if not -result_ndim <= axis < result_ndim:
            msg = 'axis {0} out of bounds [-{1}, {1})'.format(axis, result_ndim)
            raise IndexError(msg)
        if axis < 0:
            axis += result_ndim

        sl = (slice(None),) * axis + (numpy.newaxis,)
        expanded_arrays = [arr[sl] for arr in arrays]
        return numpy.concatenate(expanded_arrays, axis=axis)

if 'broadcast_to' not in dir(numpy):
    def broadcast_to(array, shape):
        shape = tuple(shape) if numpy.iterable(shape) else (shape,)
        if not shape and array.shape:
            raise ValueError('cannot broadcast a non-scalar to a scalar array')
        if any([size < 0 for size in shape]):
            raise ValueError('all elements of broadcast shape must be non-'
                             'negative')
        broadcast = numpy.nditer(
            (array,), flags=['multi_index', 'refs_ok', 'zerosize_ok'],
            op_flags=['readonly'], itershape=shape, order='C').itviews[0]
        return broadcast

# Functions new in NumPy 1.11

if 'moveaxis' not in dir(numpy):
    def moveaxis(a, source, destination):
        try:
            # allow duck-array types if they define transpose
            transpose = a.transpose
        except AttributeError:
            a = asarray(a)
            transpose = a.transpose

        source = _validate_axis(source, a.ndim, 'source')
        destination = _validate_axis(destination, a.ndim, 'destination')
        if len(source) != len(destination):
            raise ValueError('`source` and `destination` arguments must have '
                             'the same number of elements')

        order = [n for n in range(a.ndim) if n not in source]

        for dest, src in sorted(zip(destination, source)):
            order.insert(dest, src)

        result = transpose(order)
        return result
