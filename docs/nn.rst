Neural networks
===============

Penne also provides functions specific to neural networks: ``sigmoid``, ``tanh``, ``hardtanh``, ``softmax``, ``logsoftmax``, ``rectify`` (ReLU), ``crossentropy`` (log loss), ``distance2`` (mean-squared loss).

.. autofunction:: penne.sigmoid
.. autofunction:: penne.tanh
.. autofunction:: penne.hardtanh
.. autofunction:: penne.softmax
.. autofunction:: penne.logsoftmax
.. autofunction:: penne.rectify
.. autofunction:: penne.crossentropy
.. autofunction:: penne.distance2
.. autoclass:: penne.Dropout
.. autoclass:: penne.Layer
.. autoclass:: penne.EmbeddingLayer
.. autoclass:: penne.MaxoutLayer



