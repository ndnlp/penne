.. Penne documentation master file, created by
   sphinx-quickstart on Mon Jun  1 18:13:11 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Penne Documentation
===================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   expr
   model
   modules
   nn
   recurrent
   conv
   minibatch
   debug

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

