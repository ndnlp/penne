Models
======

Parameters
----------

As a slightly more complex example, suppose we want to define the
following network:

.. math::

   h &= \tanh (V i + a) \\
   o &= \tanh (W h + b) \\
   e &= \|o - c\|^2

where :math:`i` is the input vector and :math:`c` is the correct output
vector. The parameters of the model are matrices :math:`V` and :math:`W`
and vectors :math:`a` and :math:`b`.

Parameters are created like constants, using ``parameter(value)``, where
``value`` is the initial value of the parameter::

    nh = 3
    V = parameter(numpy.random.uniform(-1., 1., (nh, 2)))
    a = parameter(numpy.zeros((nh,)))
    W = parameter(numpy.random.uniform(-1., 1., (1, nh)))
    b = parameter(numpy.zeros((1,)))

Next, define a function that, given a training example, builds an
expression for the loss for that example::

    def loss(x, y, z):
        i = constant([x, y])
        c = constant([z])
        h = tanh(dot(V, i) + a)
        o = tanh(dot(W, h) + b)
        e = distance2(o, c)
        return e

    e = loss(1., 1., 0.)

Gradients
---------

Though you rarely need to do so explicitly, you can compute the
gradient of a scalar-valued expression with respect to all its
parameters::

    g = compute_gradients(e)

The result is a dict mapping from parameter objects to their
corresponding gradients.

You could then use the gradients to update the parameters, but it's
easier to use one of the built-in training algorithms, described next.

Training
--------

To train the network, first create a trainer object (here ``SGD``; see
below for other trainers). Then, feed it expressions using its
``receive`` method, which updates the parameters to try to minimize
each expression.

.. code:: python

    import random
    trainer = SGD(learning_rate=0.1)
    data = [[-1., -1., -1.], 
            [-1.,  1.,  1.],
            [ 1., -1.,  1.],
            [ 1.,  1., -1.]] * 10
    for epoch in xrange(10):
        random.shuffle(data)
        e_total = 0.
        for x, y, z in data:
            e = loss(x, y, z)
            e_total += e
            trainer.receive(e)
        print(loss/len(data))

.. parsed-literal::

    1.08034928912
    0.98879616038
    1.00183385115
    0.951137577661
    0.840384066165
    0.314003950596
    0.0539702267511
    0.0295536827621
    0.0192921979733
    0.0140214011032

Model Objects
-------------

For all but the most trivial models, you'll want to bundle the
parameters up in an object, like::

    class XOR(object):
        def __init__(self, nh):
            self.V = parameter(numpy.random.uniform(-1., 1., (nh, 2)))
            self.a = parameter(numpy.zeros((nh,)))
            self.W = parameter(numpy.random.uniform(-1., 1., (1, nh)))
            self.b = parameter(numpy.zeros((1,)))

        def loss(self, x, y, z):
            i = constant([x, y])
            c = constant([z])
            h = tanh(dot(self.V, i) + self.a)
            o = tanh(dot(self.W, h) + self.b)
            e = distance2(o, c)
            return e

Loading and Saving
------------------

To save the model, just use cPickle::

    import cPickle as pickle
    m = XOR(3)
    ...
    pickle.dump(m, open("xor.pickle", "w"))

And to load it again::

    m = pickle.load(open("xor.pickle"))

Note (for those inclined to use closures) that pickle/cPickle don't
know how to pickle closures.

Reference
---------

.. autofunction:: penne.parameter
.. autofunction:: penne.compute_gradients

.. autoclass:: penne.optimize.StochasticGradientDescent
.. autoclass:: penne.optimize.SGD
.. autoclass:: penne.optimize.AdaGrad
.. autoclass:: penne.optimize.Adagrad
.. autoclass:: penne.optimize.AdaDelta
.. autoclass:: penne.optimize.Adadelta
.. autoclass:: penne.optimize.Momentum
.. autoclass:: penne.optimize.NesterovMomentum
.. autoclass:: penne.optimize.RMSprop
.. autoclass:: penne.optimize.Adam
