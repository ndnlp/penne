Convolution and pooling
=======================

.. automodule:: penne.conv
   :members: convolve, pool, max_pool, mean_pool


