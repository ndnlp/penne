Expressions
===========

A neural network is defined as a giant expression, by creating
``constant`` or ``parameter`` objects and then performing NumPy-style
operations on them::

    x = constant(2.)
    y = constant(2.)
    z = x + y

``constant(value)`` creates an expression whose value is ``value``,
either a scalar or a NumPy array, and ``x + y`` creates a new
expression that is the sum of ``x`` and ``y``.

The expression holds both the value of the expression and how it was
computed. To get its value::

    >>> z.value
    4.0

(If, for some reason, you change the value of a `constant` or
`parameter` object and want to recompute the value of an expression
defined in terms of it, use ``compute_value(x)``.)

Arrays
------

The ``[]`` operator can be used to get an index or slice of an array. 

- Negative indices (``a[-1]``) work.
- Extended slices (``a[::2]``) work.
- Ellipses (``a[...]``) work.
- ``numpy.newaxis`` does *not* work. Use ``expand_dims`` instead.
- Assignment (``a[0] = 1.``) does *not* work. Use ``setitem`` instead::

    a = setitem(a, 0, 1.)

Note that this creates a new Expression object, copying the whole
array; avoid if possible.

Many NumPy operators and functions have equivalents in Penne that work
more or less the same way. See the reference below.

Penne has one other array operation that NumPy doesn't have:
``vecdot(x, y)`` is equivalent to ``numpy.sum(x * y)``. It takes
optional ``axis`` and ``keepdims`` arguments just like
``numpy.sum``. This especially comes in handy when dealing with
:doc:`minibatch`.

Reference
---------

.. autofunction:: penne.constant
.. autofunction:: penne.compute_value
.. autofunction:: penne.setitem

Array creation routines
.......................

.. autofunction:: penne.zeros
.. autofunction:: penne.zeros_like
.. autofunction:: penne.ones
.. autofunction:: penne.ones_like
.. autofunction:: penne.full
.. autofunction:: penne.full_like

Indexing routines
.................

.. autofunction:: penne.take
.. autofunction:: penne.where

Elementwise operations
......................

.. autofunction:: penne.add
.. autofunction:: penne.subtract
.. autofunction:: penne.multiply
.. autofunction:: penne.divide
.. autofunction:: penne.power
.. autofunction:: penne.log
.. autofunction:: penne.exp
.. autofunction:: penne.maximum
.. autofunction:: penne.minimum
.. autofunction:: penne.clip

Reductions
..........

.. autofunction:: penne.asum
.. autofunction:: penne.amax
.. autofunction:: penne.amin
.. autofunction:: penne.mean

Matrix and vector products
..........................

.. autofunction:: penne.dot
.. autofunction:: penne.matmul
.. autofunction:: penne.tensordot
.. autofunction:: penne.inner
.. autofunction:: penne.vecdot

Changing array shape or number of dimensions
............................................
.. autofunction:: penne.reshape
.. autofunction:: penne.ravel
.. autofunction:: penne.expand_dims
.. autofunction:: penne.squeeze

Transpose-like operations
.........................

.. autofunction:: penne.transpose
.. autofunction:: penne.swapaxes
.. autofunction:: penne.moveaxis

Joining arrays
..............

.. autofunction:: penne.concatenate
.. autofunction:: penne.stack
