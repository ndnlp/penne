Debugging
=========

While it is often the case that a network works perfectly on the first
try, bugs do happen. 

Call tree and expression tracebacks
-----------------------------------

If an exception is raised while computing a gradient, the usual
traceback is not very helpful. This option turns on an "expression
traceback" that shows you the line that created the offending
expression.

Turning on the call tree also changes the behavior of several other
debugging functions (see below). Unfortunately, it also slows things
down quite a bit, so only turn it on when needed.

.. autofunction:: penne.calltree.set_calltree

Visualizing expressions
-----------------------

To generate a graphical representation of an expression:

.. autofunction:: penne.viz.computation_graph

When the call tree is turned on, functions are drawn as boxes around subgraphs:

.. graphviz::

 digraph {  
    rankdir=BT;  
    node [shape=box,margin=0.1,width=0,height=0,style=filled,fillcolor=lightgrey,penwidth=0,fontname="Courier",fontsize=10];  
    edge [arrowsize=0.5,fontname="Courier",fontsize=8];  
    graph [fontname="Courier"; fontsize=8; labelloc="b"; labeljust="l"; color="lightgrey"];
    subgraph cluster_4404097424 {
    label="NPLM.__init__\l(from nplm.py:58)\l";
    subgraph cluster_4404097664 {
      label="Layer.__init__\l(from nplm.py:33)\l";
      4404416016 [label="parameter"];
      4404415936 [label="parameter"];
      }
    subgraph cluster_4404097744 {
      label="Layer.__init__\l(from nplm.py:30)\l";
      4404415616 [label="parameter"];
      4404415536 [label="parameter"];
      }
    subgraph cluster_4404097584 {
      label="EmbeddingLayer\l(from nplm.py:29)\l";
      subgraph cluster_4404097584 {
        label="Layer.__init__\l(from nn.py:289)\l";
        4404415056 [label="constant"];
        4404241184 [label="parameter"];
        4404098944 [label="parameter"];
        }
      }
    }
  subgraph cluster_4404503968 {
    label="NPLM.loss\l(from driver.py:45)\l";
    subgraph cluster_4404506448 {
      label="Layer.__call__\l(from nplm.py:42)\l";
      4404505968 [label="stack"];
      4404503328 [label="add"];
      }
    subgraph cluster_4404506128 {
      label="Layer.__call__\l(from nplm.py:42)\l";
      4404505808 [label="stack"];
      4404506368 [label="add"];
      }
    4404505888 [label="concatenate"];
    subgraph cluster_4404504768 {
      label="Layer.__call__\l(from nplm.py:44)\l";
      4404504848 [label="dot"];
      4404505488 [label="add"];
      4404505248 [label="tanh"];
      }
    subgraph cluster_4404504208 {
      label="Layer.__call__\l(from nplm.py:44)\l";
      4404503888 [label="dot"];
      4404503728 [label="add"];
      4404506048 [label="logsoftmax"];
      }
    subgraph cluster_4404504128 {
      label="crossentropy\l(from nplm.py:45)\l";
      4405279232 [label="getitem"];
      4405278592 [label="stack"];
      4405277312 [label="sum"];
      4405277792 [label="negative"];
      }
    }
  4404241184 -> 4404505968;
  4404415056 -> 4404503328;
  4404505968 -> 4404503328;
  4404098944 -> 4404505808;
  4404415056 -> 4404506368;
  4404505808 -> 4404506368;
  4404503328 -> 4404505888;
  4404506368 -> 4404505888;
  4404505888 -> 4404504848;
  4404415536 -> 4404504848;
  4404415616 -> 4404505488;
  4404504848 -> 4404505488;
  4404505488 -> 4404505248;
  4404505248 -> 4404503888;
  4404415936 -> 4404503888;
  4404416016 -> 4404503728;
  4404503888 -> 4404503728;
  4404503728 -> 4404506048;
  4404506048 -> 4405279232;
  4405279232 -> 4405278592;
  4405278592 -> 4405277312;
  4405277312 -> 4405277792;
 }

Gradient checking
-----------------

If you add your own expression operations, you can debug the automatic
differentiation by comparing it against a numerical approximation.

.. autofunction:: penne.check_gradients

Profiling
---------

If your program using using too much time or memory, you can use
standard profiling tools, but, as with tracebacks, these might not be
very helpful. Penne has a simple profiler that may be better:

.. autofunction:: penne.debug.set_profile

The text output shows the time and memory used by each expression type::

  ncalls  time (s)  mem (Mb)  location
    5652      0.39        12  dot
   11304      0.10        16  add
    8478      0.10         4  stack
    2826      0.09        10  logsoftmax
    2826      0.05         0  sum
    2826      0.04         4  concatenate
    2826      0.03         2  tanh
   16684      0.03      1497  parameter
    2826      0.03         0  negative
    2826      0.02         0  getitem
    1413      0.00         0  constant

When the call tree is enabled, you also see the usage of all functions::

  ncalls  time (s)  mem (Mb)  location
   42390      1.50        50  process
   42390      1.50        50  NPLM.loss
   28260      1.14        46  Layer.__call__
    5652      0.56        12  dot
   11304      0.30         0  crossentropy
   11304      0.25        16  add
   11304      0.25        16  Expression.__add__
    8478      0.20         4  stack
    2826      0.14        10  logsoftmax
    2826      0.12         0  sum
   18097      0.12      1497  make_model
   18097      0.12      1497  NPLM.__init__
   18097      0.12      1497  Layer.__init__
   16684      0.11      1497  parameter
    2826      0.06         0  negative
    2826      0.06         0  Expression.__neg__
    2826      0.06         2  tanh
    2826      0.06         4  concatenate
    6793      0.04         4  EmbeddingLayer
    2826      0.04         0  getitem
    2826      0.04         0  Expression.__getitem__
    1413      0.01         0  constant


The graphical output shows the call graph for the creation of all
expression objects.

.. graphviz::

 digraph {
  rankdir=TB;  node [shape=box,margin=0.1,width=0,height=0,style=filled,fillcolor=lightgrey,penwidth=0,fontname="Courier",fontsize=10];  edge [arrowsize=0.5,fontname="Courier",fontsize=8];  "constant" [label="constant\n0.01\n0"];
  "process" [label="process\n1.50\n50"];
  "logsoftmax" [label="logsoftmax\n0.14\n10"];
  "EmbeddingLayer" [label="EmbeddingLayer\n0.04\n4"];
  "sum" [label="sum\n0.12\n0"];
  "negative" [label="negative\n0.06\n0"];
  "Layer.__call__" [label="Layer.__call__\n1.14\n46"];
  "add" [label="add\n0.25\n16"];
  "crossentropy" [label="crossentropy\n0.30\n0"];
  "parameter" [label="parameter\n0.11\n1497"];
  "concatenate" [label="concatenate\n0.06\n4"];
  "Expression.__getitem__" [label="Expression.__getitem__\n0.04\n0"];
  "Expression.__add__" [label="Expression.__add__\n0.25\n16"];
  "NPLM.__init__" [label="NPLM.__init__\n0.12\n1497"];
  "stack" [label="stack\n0.20\n4"];
  "make_model" [label="make_model\n0.12\n1497"];
  "Layer.__init__" [label="Layer.__init__\n0.12\n1497"];
  "tanh" [label="tanh\n0.06\n2"];
  "Expression.__neg__" [label="Expression.__neg__\n0.06\n0"];
  "getitem" [label="getitem\n0.04\n0"];
  "dot" [label="dot\n0.56\n12"];
  "NPLM.loss" [label="NPLM.loss\n1.50\n50"];
  "NPLM.__init__" -> "EmbeddingLayer"[label="nplm.py:29\n0.04\n4"];
  "NPLM.__init__" -> "Layer.__init__"[label="nplm.py:33\n0.04\n1060"];
  "Layer.__call__" -> "logsoftmax"[label="nn.py:282\n0.14\n10"];
  "Layer.__init__" -> "parameter"[label="nn.py:251\n0.03\n4"];
  "Layer.__call__" -> "Expression.__add__"[label="nn.py:280\n0.15\n12"];
  "Layer.__call__" -> "dot"[label="nn.py:280\n0.56\n12"];
  "crossentropy" -> "Expression.__neg__"[label="nn.py:151\n0.06\n0"];
  "EmbeddingLayer" -> "Layer.__init__"[label="nn.py:289\n0.04\n4"];
  "Layer.__init__" -> "parameter"[label="nn.py:255\n0.04\n12"];
  "Expression.__neg__" -> "negative"[label="expr.py:61\n0.06\n0"];
  "crossentropy" -> "Expression.__getitem__"[label="nn.py:150\n0.04\n0"];
  "process" -> "NPLM.loss"[label="driver.py:45\n1.50\n50"];
  "NPLM.loss" -> "crossentropy"[label="nplm.py:45\n0.30\n0"];
  "NPLM.loss" -> "concatenate"[label="nplm.py:42\n0.06\n4"];
  "NPLM.loss" -> "Layer.__call__"[label="nplm.py:44\n0.91\n37"];
  "Expression.__add__" -> "add"[label="expr.py:56\n0.25\n16"];
  "crossentropy" -> "sum"[label="nn.py:151\n0.12\n0"];
  "NPLM.__init__" -> "Layer.__init__"[label="nplm.py:30\n0.04\n433"];
  "Expression.__getitem__" -> "getitem"[label="expr.py:52\n0.04\n0"];
  "Layer.__init__" -> "parameter"[label="nn.py:245\n0.04\n1481"];
  "Layer.__call__" -> "Expression.__add__"[label="nn.py:276\n0.09\n4"];
  "NPLM.loss" -> "Layer.__call__"[label="nplm.py:42\n0.23\n8"];
  "Layer.__call__" -> "tanh"[label="nn.py:282\n0.06\n2"];
  "make_model" -> "NPLM.__init__"[label="nplm.py:58\n0.12\n1497"];
  "Layer.__call__" -> "stack"[label="nn.py:273\n0.13\n4"];
  "Layer.__init__" -> "constant"[label="nn.py:257\n0.01\n0"];
  "crossentropy" -> "stack"[label="nn.py:151\n0.07\n0"];
 }

Each node has three lines: the first line is either a function or an
expression type; the second is the time spent, in seconds; the third
line is the memory allocated, in megabytes. Each edge is similarly
labeled with a line number, time, and memory usage. All quantities
include all the quantities lower in the graph.

There are a number of caveats:

- Times include only the time spent on computing values and gradients
  of expressions (Expression.forward and backward); everything else is
  uncounted.

- Memory usages are just the sum of the sizes of all computed
  arrays. Sometimes memory is shared between arrays; the profiler
  doesn't know about this and double-counts the arrays. Also, the
  profiler doesn't know anything about freeing memory, so it might not
  present an accurate picture of actual usage at any point in time.
