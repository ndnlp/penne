Introduction
============

This is a library that tries to make creating neural networks as easy
as possible by being as similar to Python/NumPy as possible. It
borrows heavily from Theano_ and CNN_. If you find it useful, or
want to help improve it, please let me (David Chiang) know!

See ``README.md`` for installation instructions.

Loading
-------

To load the package (note, don't ``import * from`` both ``numpy`` and
``penne``, as they have many symbols in common)::

    import numpy
    from penne import *

GPU support
-----------

If you want to enable the GPU, call ``use_gpu()``. This function takes a single argument, a device name:

- ``"cuda"`` (default): first available CUDA device
- ``"cuda0"``, ``"cuda1"``, etc.: CUDA device 0, 1, etc.
- ``"opencl0:1"``: OpenCL platform 0 device 1

See the documentation for ``pygpu.init()`` for more information.

.. _Theano: http://deeplearning.net/software/theano/
.. _CNN: (https://github.com/clab/cnn)

