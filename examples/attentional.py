"""Attentional translation model.

Bahdanau et al., 2014. Neural machine translation by jointly learning
to align and translation. In Proc. ICLR 2015. 
https://arxiv.org/abs/1409.0473"""

from __future__ import division
import numpy
from penne import *
from penne import recurrent, text

class Attentional(object):
    """Attentional translation model.

    :param embedding_size: Size of source word embeddings
    :param hidden_size: Number of hidden units
    :param output_size: Number of output units
    :type reader: penne.text.MultitextReader
    """

    def __init__(self, embedding_size, hidden_size, output_size, reader):
        self.reader = reader
        self.encoder = Encoder(len(reader[0].nr), embedding_size, hidden_size)
        self.decoder = Decoder(len(reader[1].nr), embedding_size, hidden_size, output_size)

    def decode_loss(self, enc, ewords):
        self.decoder.start(enc)
        l = constant(0.)
        e_prev = ewords[0]
        for e in ewords[1:]:
            o = self.decoder.step(e_prev)
            l += crossentropy(o, e)
            e_prev = e
        return l

    def decode_greedy(self, enc, max_words=50):
        ewords = []
        self.decoder.start(enc)
        e = self.reader[1].nr.num('<s>')
        stop = self.reader[1].nr.num('</s>')
        while e != stop and len(ewords) < max_words:
            o = self.decoder.step(e)
            e = numpy.argmax(o.value)
            ewords.append(e)
        return ewords

    def loss(self, fwords, ewords):
        return self.decode_loss(self.encoder.encode(fwords), ewords)

    def translate(self, fwords):
        return self.decode_greedy(self.encoder.encode(fwords))

class Encoder(object):
    """Bidirectional GRU encoder."""
    def __init__(self, vocab_size, embedding_size, hidden_size):
        self.embedding = recurrent.Map(EmbeddingLayer(vocab_size, embedding_size))
        self.forward = recurrent.GRU(embedding_size, hidden_size)
        self.backward = recurrent.GRU(embedding_size, hidden_size)

    def encode(self, x):
        """Encode an input sentence.

        :param x: input words
        :type x:  list of M-ndarrays of ints
        :return:  (Expression for) array of encodings
        :rtype:   (Expression for) (M,N,D)-ndarray of floats

        where 
        M is the minibatch size
        N is the (maximum) input sentence length
        D is the embedding size
        """

        mask = numpy.expand_dims(numpy.asarray(x) >= 0, -1)

        # From section A.2.1

        # Embed words
        Ex = self.embedding.transduce(x, mask)

        # Run GRU
        h_forward = self.forward.transduce(Ex[:-1], mask[:-1])
        h_backward = self.backward.transduce(Ex[1:], mask[1:], reverse=True)

        h_bridge = h_backward[0]

        # Pack the vectors into a single array
        h_forward = stack(h_forward, axis=-2)
        h_backward = stack(h_backward, axis=-2)
        h = concatenate([h_forward, h_backward], axis=-1) # (7)

        mask = numpy.delete(mask, (0), axis=0) # Remove extra initial column caused by initialization

        return h_bridge, h, numpy.rollaxis(mask, -2)

class Decoder(recurrent.Transducer):
    """Attention model and decoder."""
    def __init__(self, vocab_size, embedding_size, hidden_size, output_size):
        self.hidden_size = hidden_size

        self.a = Stack(Layer([hidden_size, hidden_size*2], hidden_size),
                       Layer(hidden_size, 1, None))

        self.init = Layer(hidden_size, hidden_size)

        self.embedding = EmbeddingLayer(vocab_size, embedding_size)
        self.recurrent = recurrent.GRU([embedding_size, hidden_size*2], hidden_size)
        self.output = Stack(MaxoutLayer([hidden_size, embedding_size, hidden_size*2], output_size),
                            Layer(output_size, vocab_size, logsoftmax))

    def start(self, enc):
        """Initialization of decoder.

        :param h: (Expression for) encoding."""

        h_bridge, h, h_mask = enc
        s0 = self.init(h_bridge)
        self.recurrent.start(s0)
        self.h = h
        self.h_mask = h_mask

    def context(self):
        """Attention model.

        :return: (Expression for) context vector
        :rtype:  (Expression for) (M,N,D)-ndarray of floats

        where 
        M is the minibatch size
        N is the (maximum) input sentence length
        D is the embedding size
        """
        s = self.recurrent.state()
        e = self.a(expand_dims(s, -2), self.h)           # eq. after (6)
        e = where(self.h_mask, e, constant(-numpy.inf))  # mask out fillvalues
        alpha = softmax(e, axis=-2)                      # (6)
        c = vecdot(alpha, self.h, axis=-2)               # (5)
        return c

    def step(self, y):
        """Decoder.

        :param y: Previous output word
        :type y:  M-ndarray of ints
        :return:  (Expression for) Probability distribution over next word
        :rtype:   (Expression for) (M,V)-ndarray of floats

        where 
        M is the minibatch size
        V is the vocabulary size
        """

        # From section A.2.2
        c = self.context()
        Ey = self.embedding(y)
        s = self.recurrent.step(Ey, c)
        logp = self.output(s, Ey, c)
        return logp

if __name__ == "__main__":
    import driver
    import argparse

    argparser = argparse.ArgumentParser(description="Train/test an attentional translation model.")
    driver.add_arguments_to(argparser, 2)
    argparser.add_argument('--minibatch', dest='minibatch', type=int, metavar='n', default=1, help='use minibatches of size n (default 1)')
    argparser.add_argument('--midibatch', dest='midibatch', type=int, metavar='n', help='use midibatches (sorted by length) of size n (default no midibatches)')
    argparser.add_argument('--vocab', dest='vocab', metavar='n', type=int, help='limit vocabulary size')
    argparser.add_argument('--embedding', dest='embedding', metavar='n', type=int, default=620, help='hidden units (default 620)')
    argparser.add_argument('--hidden', dest='hidden', metavar='n', type=int, default=1000, help='hidden units (default 1000)')
    argparser.add_argument('--output', dest='output', metavar='n', type=int, default=500, help='output units (default 500)')
    args = argparser.parse_args()
    
    def make_model():
        reader = text.MultitextReader.from_files(args.train, vocab_size=args.vocab)
        return Attentional(args.embedding, args.hidden, args.output, reader)

    trainer = SGD(learning_rate=0.1, clip_gradients=5.)
    def train(model, filenames):
        l = n = 0
        data = model.reader.read_minibatches(filenames, shuffle=True, 
                                             minibatch_size=args.minibatch,
                                             midibatch_size=args.midibatch, 
                                             max_words=50)
        for ibatch, obatch in data:
            bl = model.loss(ibatch, obatch)
            l += bl.value
            trainer.receive(bl / constant(args.minibatch))
            n += sum(model.reader[1].minibatch_lengths(obatch))
        return {'ppl': numpy.exp(l/n), 'n': n}

    def test(model, infilenames, outfilename=None):
        import bleu
        data = model.reader.read_lines(infilenames)
        outputs = []
        b = bleu.zero()
        n = 0
        for input, ref in data:
            output = model.translate(input)
            outputs.append(output)
            b += bleu.count(output, ref)
            n += len(output)+1
        if outfilename:
            with open(outfilename, "w") as outfile:
                for output in outputs:
                    outfile.write(model.reader[1].line_to_str(output) + "\n")
        b = bleu.score(b)
        return {'bleu': b, 'valid': -b, 'n': n}

    driver.main(args, make_model, train, test)
