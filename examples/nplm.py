"""
Feedforward language model.
Bengio et al., 2003. A neural probabilistic language model. JMLR 3:1137-1155.
"""

from __future__ import division
from six.moves import range
import numpy
from penne import *
from penne import text

class NPLM(object):
    """Neural probabilistic language model.
    :param order:          Size of n-grams
    :param embedding_size: Number of embedding units
    :param depth:          Number of hidden layers
    :param hidden_size:    Number of hidden units
    :type reader:          text.TextReader
    """
    def __init__(self, order, embedding_size, depth, hidden_size, reader):
        self.reader = reader

        self.order = order
        self.input_layer = EmbeddingLayer(len(reader.nr), embedding_size)
        self.layers = [Layer(embedding_size*(order-1), hidden_size)]
        while len(self.layers) < depth:
            self.layers.append(Layer(hidden_size, hidden_size))
        self.layers.append(Layer(hidden_size, len(reader.nr), logsoftmax))

    def loss(self, ngram):
        """Compute loss function for n-gram.
        :param ngram:  Minibatch of ngrams
        :type ngram:   list of list of ints
        :rtype:        Expression
        """
        v = concatenate([self.input_layer(w) for w in ngram[:-1]], axis=-1)
        for layer in self.layers:
            v = layer(v)
        return crossentropy(v, ngram[-1])

if __name__ == "__main__":
    import driver
    import argparse

    argparser = argparse.ArgumentParser(description="Train/test a neural n-gram language model.")
    driver.add_arguments_to(argparser)
    argparser.add_argument('--minibatch', dest='minibatch', type=int, metavar='n', default=1, help='use minibatches of size n (default 1)')
    argparser.add_argument('--midibatch', dest='midibatch', type=int, metavar='n', help='use midibatches (sorted by length) of size n (default no midibatches)')
    argparser.add_argument('--order', dest='order', metavar='n', type=int, default=3, help='n-gram size (default 3)')
    argparser.add_argument('--vocab', dest='vocab', metavar='n', type=int, help='limit vocabulary size')
    argparser.add_argument('--embedding', dest='embedding', metavar='n', type=int, default=100, help='embedding size (default 100)')
    argparser.add_argument('--depth', dest='depth', metavar='n', type=int, default=1, help='depth (default 1)')
    argparser.add_argument('--hidden', dest='hidden', metavar='n', type=int, default=100, help='hidden units (default 100)')
    args = argparser.parse_args()

    def make_model():
        reader = text.TextReader.from_file(args.train[0], vocab_size=args.vocab)
        return NPLM(args.order, args.embedding, args.depth, args.hidden, reader)

    trainer = SGD(learning_rate=0.1, clip_gradients=5.)
    def train(model, filenames):
        l = n = 0
        data = model.reader.read_ngram_minibatches(filenames[0], model.order, shuffle=True, 
                                                   minibatch_size=args.minibatch,
                                                   midibatch_size=args.midibatch)
        for batch in data:
            bl = model.loss(batch)
            l += bl.value
            trainer.receive(bl / constant(args.minibatch))
            n += batch.shape[1]
        return {'ppl': numpy.exp(l/n), 'n': n}

    def test(model, filenames, _=None):
        l = n = 0
        data = model.reader.read_ngram_minibatches(filenames[0], model.order, 
                                                   minibatch_size=args.minibatch)
        for batch in data:
            l += model.loss(batch).value
            n += batch.shape[1]
        ppl = numpy.exp(l/n)
        return {'ppl': ppl, 'valid': ppl, 'n': n}

    driver.main(args, make_model, train, test)
