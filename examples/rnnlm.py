"""
Deep recurrent language model.
"""

from __future__ import division
from six.moves import range
import numpy
from penne import *
from penne import recurrent, text

class RNNLM(object):
    """RNN language model.
    :param embedding_size: Number of embedding dimensions
    :param depth:       Number of stacked RNN layers
    :param hidden_size: Number of hidden units
    :type reader:   text.TextReader
    """
    def __init__(self, embedding_size, depth, hidden_size, reader):
        self.reader = reader

        # Words (one-hot) to word embeddings
        layers = [recurrent.Map(EmbeddingLayer(len(reader.nr), embedding_size))]
        # LSTM layers
        self.dropout = Dropout(0.3)
        prev_size = embedding_size
        for i in range(depth):
            layers.append(recurrent.LSTM(prev_size, hidden_size))
            layers.append(recurrent.Map(self.dropout))
            prev_size = hidden_size
        # Output layer
        layers.append(recurrent.Map(Layer(prev_size, len(reader.nr), logsoftmax)))
        self.rnn = recurrent.Stack(*layers)

    def loss(self, sent):
        """Compute loss function for sentence.
        :param sent:  Minibatch of sentences, each ending with stop symbol.
        :type sent:   list of list of ints
        :rtype:       Expression
        """

        mask = numpy.expand_dims(numpy.asarray(sent) >= 0, -1)
        predict = self.rnn.transduce(sent[:-1], mask[:-1])
        loss = constant(0.)
        for o, c in zip(predict, sent[1:]):
            loss += crossentropy(o, c)
        return loss

    def sample(self):
        """Randomly sample a sentence."""
        self.rnn.start()
        sent = []
        w = self.reader.nr.num('<s>')
        stop = self.reader.nr.num('</s>')
        while True:
            o = self.rnn.step(w)
            w = numpy.argmax(numpy.random.multinomial(1, numpy.exp(o.value)))
            if w == self.stop: break
            sent.append(w)
        return sent

if __name__ == "__main__":
    import driver
    import argparse

    argparser = argparse.ArgumentParser(description="Train/test a RNN language model.")
    driver.add_arguments_to(argparser)
    argparser.add_argument('--minibatch', dest='minibatch', type=int, metavar='n', default=1, help='use minibatches of size n (default 1)')
    argparser.add_argument('--midibatch', dest='midibatch', type=int, metavar='n', help='use midibatches (sorted by length) of size n (default no midibatches)')
    argparser.add_argument('--vocab', dest='vocab', metavar='n', type=int, help='limit vocabulary size')
    argparser.add_argument('--depth', dest='depth', metavar='n', type=int, default=2, help='depth (default 2)')
    argparser.add_argument('--embedding', dest='embedding', metavar='n', type=int, default=1500, help='embedding size (default 1500)')
    argparser.add_argument('--hidden', dest='hidden', metavar='n', type=int, default=1500, help='hidden units (default 1500)')
    args = argparser.parse_args()

    def make_model():
        reader = text.TextReader.from_file(args.train[0], vocab_size=args.vocab)
        return RNNLM(args.embedding, args.depth, args.hidden, reader)

    trainer = SGD(learning_rate=0.1, clip_gradients=5.)
    def train(model, filenames):
        l = n = 0
        data = model.reader.read_minibatches(filenames[0], shuffle=True, 
                                             minibatch_size=args.minibatch,
                                             midibatch_size=args.midibatch)
        model.dropout.enable()
        for batch in data:
            bl = model.loss(batch)
            l += bl.value
            trainer.receive(bl / constant(args.minibatch))
            n += sum(model.reader.minibatch_lengths(batch)-1) # -1 for <s>
        model.dropout.disable()
        return {'ppl': numpy.exp(l/n), 'n': n}

    def test(model, filenames, _=None):
        l = n = 0
        data = model.reader.read_minibatches(filenames[0], minibatch_size=args.minibatch)
        for batch in data:
            l += model.loss(batch).value
            n += sum(model.reader.minibatch_lengths(batch)-1) # -1 for <s>
        ppl = numpy.exp(l/n)
        return {'ppl': ppl, 'valid': ppl, 'n': n}

    driver.main(args, make_model, train, test)
