from six import iteritems

import sys
import time
import cPickle as pickle
import logging

import penne

def save_model(model, filename):
    if sys.version_info >= (3, 3):
        from os import replace
    elif sys.platform == "win32":
        from osreplace import replace
    else:
        from os import rename as replace

    m = sys.getrecursionlimit()
    try:
        sys.setrecursionlimit(max(m, 100000))
        with open(filename+".tmp", "w") as outfile:
            pickle.dump(model, outfile, protocol=pickle.HIGHEST_PROTOCOL)
        replace(filename+".tmp", filename)
    finally:
        sys.setrecursionlimit(m)

def add_arguments_to(argparser, num_files=1):
    """Add arguments to an ArgumentParser.

    :type argparser: argparse.ArgumentParser
    :param num_files: Number of filename arguments
    :type num_files: int"""

    argparser.add_argument('--train', dest='train', nargs=num_files, metavar='filename', help='training file')
    argparser.add_argument('--valid', dest='valid', nargs=num_files, metavar='filename', help='validation file')
    argparser.add_argument('--valid-out', dest='valid_out', metavar='filename', help='translate validation file to <filename>')
    argparser.add_argument('--test', dest='test', nargs=num_files, metavar='filename', help='testing file')
    argparser.add_argument('--test-out', dest='test_out', metavar='filename', help='translate test file to <filename>')
    argparser.add_argument('--load', dest='load', metavar='filename', help='load model from file')
    argparser.add_argument('--save', dest='save', metavar='filename', help='save model to file')
    argparser.add_argument('--gpu', dest='gpu', metavar='device', nargs='?', const='cuda', help='use GPU')
    argparser.add_argument('--epochs', dest='epochs', type=int, metavar='n', default=1, help='number of epochs (default 1)')
    argparser.add_argument('--profile', dest='profile', metavar='filename', help='profile and write text output to filename')
    argparser.add_argument('--profile-graph', dest='profile_graph', metavar='filename', help='profile and write graph in DOT format to filename')
    argparser.add_argument('--call-tree', dest='calltree', action='store_true', help='enable call tree')

def format_stats(d):
    return ' '.join('{}={}'.format(k,v) for (k,v) in iteritems(d))

def main(args, make_model, train, test):
    """Driver program for Penne examples.

    Before calling this function, you must call driver.add_arguments_to.

    :param make_model: Create model object from arguments
    """

    if args.gpu:
        penne.use_gpu(args.gpu)

    if args.profile:
        penne.debug.set_profile(file=args.profile)
    if args.profile_graph:
        penne.debug.set_profile(graph_file=args.profile_graph)
    if args.calltree:
        penne.debug.set_calltree()

    logger = logging.getLogger('penne')
    logger.addHandler(logging.StreamHandler(sys.stdout))
    logger.setLevel(logging.INFO)

    if args.load:
        model = pickle.load(open(args.load))
    elif args.train:
        model = make_model()
    else:
        sys.exit("No model; you must use either --train or --load.\n")

    if args.train:
        if args.valid: best_valid_score = float("inf")
        for epoch in range(args.epochs):
            print("epoch {}".format(epoch+1))

            t0 = time.time()
            train_stats = train(model, args.train)
            t = time.time()-t0
            n = train_stats.pop('n', 1)
            print("  train: time={} speed={} {}".format(t, n/t, format_stats(train_stats)))
            sys.stdout.flush()

            if args.valid:
                t0 = time.time()
                valid_stats = test(model, args.valid, args.valid_out)
                t = time.time()-t0
                n = valid_stats.pop('n', 1)
                valid_score = valid_stats.pop('valid', 0.)
                print("  valid: time={} speed={} {}".format(t, n/t, format_stats(valid_stats)))
                if valid_score < best_valid_score:
                    if args.save:
                        print("  saving model")
                        save_model(model, args.save)
                    best_valid_score = valid_score
                sys.stdout.flush()

    if not args.valid and args.save:
        print("  saving model")
        save_model(model, args.save)

    if args.test:
        if args.valid and args.save:
            print("  loading best model")
            model = pickle.load(open(args.save))

        t0 = time.time()
        test_stats = test(model, args.test, args.test_out)
        t = time.time()-t0
        n = test_stats.pop('n', 1)
        print("  test: time={} speed={} {}".format(t, n/t, format_stats(test_stats)))
