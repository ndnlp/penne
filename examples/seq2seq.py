"""Sutskever, Vinyals, and Le, 2015. Sequence-to-sequence leanring
with neural networks. In Proc. NIPS 2014.

https://papers.nips.cc/paper/5346-sequence-to-sequence-learning-with-neural-networks.pdf"""

from __future__ import division
from six.moves import range
import numpy
from penne import *
from penne import recurrent, text

class SequenceToSequence(object):
    """Sequence-to-sequence translation model.
    :param embedding_size: Number of embedding dimensions
    :param depth:       Number of stacked RNN layers
    :param hidden_size: Number of hidden units
    :type fnumberizer:  Numberizer for source language
    :type enumberizer:  Numberizer for target language
    """

    def __init__(self, embedding_size, depth, hidden_size, reader):
        self.reader = reader

        ## Encoder
        layers = [recurrent.Map(EmbeddingLayer(len(reader[0].nr), embedding_size))]
        prev_size = embedding_size
        for i in range(depth):
            layers.append(recurrent.LSTM(prev_size, hidden_size))
            prev_size = hidden_size
        self.encoder = recurrent.Stack(*layers)

        ## Decoder
        layers = [recurrent.Map(EmbeddingLayer(len(reader[1].nr), embedding_size))]
        prev_size = embedding_size
        for i in range(depth):
            layers.append(recurrent.LSTM(prev_size, hidden_size))
            prev_size = hidden_size
        self.decoder = recurrent.Stack(*layers)
        self.output_layer = Layer(prev_size, len(reader[1].nr), logsoftmax)

    def encode(self, x):
        mask = numpy.expand_dims(numpy.asarray(x) >= 0, -1)
        self.encoder.transduce(x[1:], mask[1:], reverse=True)
        return self.encoder.state()

    def decode_loss(self, state, ewords):
        self.decoder.start(state)
        l = constant(0.)
        e_prev = ewords[0]
        for e in ewords[1:]:
            o = self.output_layer(self.decoder.step(e_prev))
            l += crossentropy(o, e)
            e_prev = e
        return l

    def decode_greedy(self, state, max_words=50):
        ewords = []
        self.decoder.start(state)
        e = self.reader[1].nr.num('<s>')
        stop = self.reader[1].nr.num('</s>')
        while e != stop and len(ewords) < max_words:
            o = self.output_layer(self.decoder.step(e))
            e = numpy.argmax(o.value)
            ewords.append(e)
        return ewords

    def loss(self, fwords, ewords):
        return self.decode_loss(self.encode(fwords), ewords)
    
    def translate(self, fwords):
        return self.decode_greedy(self.encode(fwords))

if __name__ == "__main__":
    import driver
    import argparse

    argparser = argparse.ArgumentParser(description="Train/test a sequence-to-sequence translation model.")
    driver.add_arguments_to(argparser, 2)
    argparser.add_argument('--minibatch', dest='minibatch', type=int, metavar='n', default=1, help='use minibatches of size n (default 1)')
    argparser.add_argument('--midibatch', dest='midibatch', type=int, metavar='n', help='use midibatches (sorted by length) of size n (default no midibatches)')
    argparser.add_argument('--vocab', dest='vocab', metavar='n', type=int, help='limit vocabulary size')
    argparser.add_argument('--depth', dest='depth', metavar='n', type=int, default=4, help='depth (default 4)')
    argparser.add_argument('--embedding', dest='embedding', metavar='n', type=int, default=1000, help='embedding size (default 1000)')
    argparser.add_argument('--hidden', dest='hidden', metavar='n', type=int, default=1000, help='hidden units (default 1000)')
    args = argparser.parse_args()

    def make_model():
        reader = text.MultitextReader.from_files(args.train, vocab_size=args.vocab)
        return SequenceToSequence(args.embedding, args.depth, args.hidden, reader)

    trainer = SGD(learning_rate=0.1, clip_gradients=5.)
    def train(model, filenames):
        l = n = 0
        data = model.reader.read_minibatches(filenames, shuffle=True, 
                                             minibatch_size=args.minibatch,
                                             midibatch_size=args.midibatch, 
                                             max_words=50)
        for ibatch, obatch in data:
            bl = model.loss(ibatch, obatch)
            l += bl.value
            trainer.receive(bl / constant(args.minibatch))
            n += sum(model.reader[1].minibatch_lengths(obatch))
        return {'ppl': numpy.exp(l/n), 'n': n}

    def test(model, infilenames, outfilename=None):
        import bleu
        data = model.reader.read_lines(infilenames)
        outputs = []
        b = bleu.zero()
        n = 0
        for input, ref in data:
            output = model.translate(input)
            outputs.append(output)
            b += bleu.count(output, ref)
            n += len(output)+1
        if outfilename:
            with open(outfilename, "w") as outfile:
                for output in outputs:
                    outfile.write(model.reader[1].line_to_str(output) + "\n")
        b = bleu.score(b)
        return {'bleu': b, 'valid': -b, 'n': n}

    driver.main(args, make_model, train, test)
