# Python Easy Neural Network Extruder

This is a library that tries to make creating neural networks as easy
as possible by being as similar to Python/NumPy as possible. It
borrows heavily from [Theano] and [CNN]. If you find it useful, or
want to help improve it, please let me (David Chiang) know!

- [Documentation](https://nlp.nd.edu/penne/docs)
- [Source code](https://bitbucket.org/ndnlp/penne)

## Installing

You need to have NumPy (>= 1.9) and six (>= 1.10). Having SciPy (>=
0.14) enables n-dimensional convolutions and a few other things.

```
pip install numpy scipy six
git clone https://bitbucket.org/ndnlp/penne.git
cd penne
python setup.py install
```

## Building and installing libgpuarray

There is also experimental GPU support, which requires the development
version of [libgpuarray]. See [libgpuarray's
documentation](http://deeplearning.net/software/libgpuarray/installation.html)
for detailed instructions, but here is the short version:

Install the [CUDA toolkit](https://developer.nvidia.com/cuda-toolkit)
and make sure that your PATH includes nvcc. OpenCL should work as
well. Note that if the build process doesn't find either one, it will
issue a warning, build successfully, and nothing will work!

This is what I used (replacing `/usr/local` with wherever you want the
library to be installed):

```
pip install numpy cython mako nose
git clone https://github.com/Theano/libgpuarray
cd libgpuarray
cmake -DCMAKE_INSTALL_PREFIX=/usr/local
make install
CFLAGS=-Isrc LDFLAGS=-L/usr/local/lib python setup.py install
```

Then be sure to set your `LD_LIBRARY_PATH` to include both the CUDA
and libgpuarray libraries.

## Contributors

- David Chiang (University of Notre Dame)
- Ke Tran (University of Amsterdam)

## License

Penne is open-source software under the MIT License. See the file
LICENSE for more information.

[Theano]: http://deeplearning.net/software/theano/
[libgpuarray]: https://github.com/Theano/libgpuarray
[CNN]: (https://github.com/clab/cnn)

